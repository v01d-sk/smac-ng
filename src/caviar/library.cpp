#include "caviar.h"
#include "assert.h"

void Caviar::Library::destroy()
{
	if (s_inst == nullptr)
	{
		return;
	}
	// alternatively, unique_ptr may be used for ownership/autodeletion.
	for (auto& name_and_palette: s_inst->m_palette)
	{
		delete name_and_palette.second;
	}
	for (auto& name_and_model: s_inst->m_model)
	{
		delete name_and_model.second;
	}
	delete s_inst;
}

void Caviar::Library::model(const std::string & name, Caviar::Model * model)
{
	assert(model);

	if (m_model.find(name) != m_model.end())
	{
		fprintf(stderr, "Model with name '%s' already exists!\n", name.c_str());
		return;
	}

	m_model.insert(std::make_pair(name, model));
	m_pModelVector.push_back(model);
}

Caviar::Model * Caviar::Library::model(const std::string & name) const
{
	NameModelMap::const_iterator it = m_model.find(name);

	if (it == m_model.end())
	{
		fprintf(stderr, "Unknown model '%s'!\n", name.c_str());
		return nullptr;
	}

	return it->second;
}

bool Caviar::Library::isModel(const std::string & name) const
{
	return (m_model.find(name) != m_model.end());
}

void Caviar::Library::palette(const std::string & name, Caviar::Palette * palette)
{
	assert(palette);

	if (m_palette.find(name) != m_palette.end())
	{
		fprintf(stderr, "Palette with name '%s' already exists!\n", name.c_str());
		return;
	}

	m_palette.insert(std::make_pair(name, palette));
}

Caviar::Palette * Caviar::Library::palette(const std::string & name) const
{
	NamePaletteMap::const_iterator it = m_palette.find(name);

	if (it == m_palette.end())
	{
		fprintf(stderr, "Unknown palette '%s'!\n", name.c_str());
		return nullptr;
	}

	return it->second;
}

bool Caviar::Library::isPalette(const std::string & name) const
{
	return (m_palette.find(name) != m_palette.end());
}
