#include "caviar.h"
#include <cstdio>

int main(int argc, char ** argv)
{
	if (argc == 1)
	{
		printf("Usage: %s <file_name> <file_name_2> ...\n"
				"<file_name>    - caviar model file name\n\n"
				"Pass as many model names as you wish to load into library.\n\n"
				"Will try to decode caviar model and print some info about it\n", argv[0]
			  );
		return 0;
	}

	for (int q = 1; q < argc; ++q)
	{
		std::string f_name(argv[q]);
		Caviar::open(f_name);
	}

	return 0;
}
