#include "caviar.h"
#include "caviar_int.h"
#include <cstdio>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <stddef.h>
#include <math.h>

#undef CAVIAR_SWAP_ENDIANNESS

#ifdef __BYTE_ORDER__
#	if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#		define CAVIAR_SWAP_ENDIANNESS
#	endif
#endif
#ifdef __FLAT_WORD_ORDER__
#	if __FLAT_WORD_ORDER__ == __ORDER_BIG_ENDIAN__
#		define CAVIAR_SWAP_ENDIANNESS
#	endif
#endif
#ifdef __BIG_ENDIAN__
#	define CAVIAR_SWAP_ENDIANNESS
#endif

#ifdef CAVIAR_SWAP_ENDIANNESS

#define CAVIAR_DATA_16(x) (((x) >> 8) | ((x) << 8))
#define CAVIAR_DATA_32(x) (((x) >> 24) | (((x) & 0x00FF0000) >> 8) | (((x) & 0x0000FF00) << 8) | ((x) << 24))

#else

#define CAVIAR_DATA_16(data)	(data)
#define CAVIAR_DATA_32(data)	(data)

#endif

using namespace Caviar;

/** Parse context container.
 * Contains information related to current state of parser.
 * Parsing is based on assumption that names are unique and thus
 * can be used as identifiers.
 */
struct ParseContext {
	Caviar::Palette * 	m_palette;			///< palette used (either decoded or loaded from library)
	Caviar::Model * 	m_model;			///< model accessed
	Caviar::Library * 	m_library;			///< pointer to model library
	unsigned			m_depth;			///< parser container depth
	std::string			m_lastPaletteName;	///< last palette name decoded (used to tell if palette has to be decoded or can be re-used)
};

/** Utility function to pretty format output.
 * Useful for debugging parser.
 * @param ctxt parse context
 * @param f file where output goes
 */
void indent(ParseContext * ctxt, FILE * f = stdout)
{
	for (unsigned q = 0; q < ctxt->m_depth; ++q)
	{
		fprintf(f, "  ");
	}
}

bool parse_caviar_chunk(Caviar::ChunkHdr * chunk_start, unsigned chunk_size, ParseContext * lib);

bool Caviar::open(const std::string & f_name)
{
	int f = ::open(f_name.c_str(), O_RDONLY);

	if (f < 0)
	{
		fprintf(stderr, "Unable to open file %s!\n", f_name.c_str());
		return false;
	}

	unsigned char * buffer = nullptr;

	off_t file_size = lseek(f, 0, SEEK_END);
	lseek(f, 0, SEEK_SET);

	if (file_size == -1)
	{
		fprintf(stderr, "Unable to get model size!\n");
		close(f);
		return false;
	}

	buffer = (unsigned char *) malloc(file_size);

	if (!buffer)
	{
		fprintf(stderr, "Unable to allocate file buffer!\n");
		close(f);
		return false;
	}

	off_t remaining = file_size;

	while (remaining)
	{
		printf("Reading %ld bytes at offset %ld\t", remaining, file_size - remaining);
		int rbytes = read(f, (void *) &buffer[file_size - remaining], remaining);
		if (rbytes > 0)
		{
			printf("read %d bytes\n", rbytes);
			remaining -= rbytes;
		}

		if (rbytes < 0)
		{
			fprintf(stderr, "Unable to read %ld bytes from file!\n", remaining);
			close(f);
			return false;
		}

//		sleep(1);
	}

	close(f);

	Caviar::Library * cvr = Caviar::Library::get();

	ParseContext ctx = { nullptr, nullptr, cvr, 0, "" };

	parse_caviar_chunk((ChunkHdr *) &buffer[0], file_size, &ctx);
	free(buffer);
	return true;
}

// This would compute faster on C3TUH computer
/** De-compose voxel's direction element into 3D coordinates leading to next voxel position.
 * @param direction direction element extracted from voxel
 * @param offsets 4-element array, IF offsets[4] == 0 THEN offsets[0 .. 2] contain 
 * coordinate offsets to next voxel (dx, dy, dz). IF element 4 == 1 THEN this is last
 * voxel in chain and there is no next voxel.
 * @note This would compute faster on C3TUH computer.
 */
int relative_position(uint8_t direction, int8_t offsets[4])
{
	direction = direction >> 3;
	if (direction > 0x0C) direction += 1; // Adjustment for missing [0,0,0] relative offset
	if (direction > 0x1B)
	{
		return -1;
	}
	for (int q = 3; q >= 0; --q)
	{
		offsets[q] = direction % 3;
		direction = direction / 3;
	}

	for (int q = 1; q < 4; ++q)
	{
		offsets[q] -= 1;
		// asserted here? My code for conversion from binary into ternary is not working properly
		assert( -1 <= offsets[q] && offsets[q] <= 1);
	}
	return 0;
}

// This one was shamelessly stolen from:
// http://alphacentauri2.info/w/index.php?title=Caviar
/** De-compose voxel's normal element into normal vector.
 * @param n voxel normal element
 * @param x normal vector x element
 * @param y normal vector y element
 * @param z normal vector z element
 */
void decode_normal(uint16_t n, double* x, double* y, double* z)
{
	// A1(0-23)*89+A2(0-88)
	// Normally the highest number would be 23 * 89 + 88 = 2136,
	// but because in the case of A1=0, we know Y=1.0, so X=Z=0.0, so A2 = 0,
	// we compressed the range 0-88 down to 0
	// In the case of A1=0, sin(A1)=0, so x & z will be 0 for any value
	// from 0 to 88 below
	n = n + 88;

	// extract the original angles
	// Angle1 = Z to X-axis
	// Angle2 = Y to X-axis
	uint16_t dwAngle1 = 88 - n % 89;
	uint16_t dwAngle2 = n / 89;

	// convert the angles to radians
	double dAngle1 = dwAngle1 / 88.0 * 2.0 * M_PI;
	double dAngle2 = dwAngle2 / 23.0 * M_PI / 2.0;

	// convert the angles to coords in the range [-1, 1]
	*z = sin(dAngle1) * sin(dAngle2);
	*x = cos(dAngle1) * sin(dAngle2);
	*y = cos(dAngle2);
}

/** Parse chunk containing multiple sub-chunks. Call chunk parser for each sub-chunk.
 * @param chunk_start pointer to container chunk start
 * @param chunk_size size of container chunk
 * @param ctxt pointer to parse context
 * @return TRUE if parser finished successfully FALSE on error
 */
bool parse_container_chunk(Caviar::ChunkHdr * chunk_start, unsigned chunk_size, ParseContext * ctxt)
{
	indent(ctxt);
	printf("Parsing chunk container of size %d\n", chunk_size);
	Caviar::ChunkHdr * chunk_child = chunk_start + 1;
	while (chunk_child < (Caviar::ChunkHdr *) (((char *) chunk_start + chunk_size))) {
		indent(ctxt);
		printf("Parsing child chunk at offset %ld of size %d\n", (char *) chunk_child - (char *) chunk_start, CAVIAR_DATA_32(chunk_child->size));
		if (!parse_caviar_chunk(chunk_child, CAVIAR_DATA_32(chunk_child->size), ctxt))
			return false;
		chunk_child = (Caviar::ChunkHdr *) (((char *) chunk_child) + CAVIAR_DATA_32(chunk_child->size));
	}

	return true;
}

/** Parse chunk containing single Caviar data entity.
 * @param chunk_start pointer to start of FILE_CHUNK within buffer
 * @param chunk_size size of chunk
 * @param ctxt pointer to parse context
 * @return TRUE if parser finished successfully FALSE on error
 */
bool parse_caviar_chunk(Caviar::ChunkHdr * chunk_start, unsigned chunk_size, ParseContext * ctxt)
{
	ctxt->m_depth++;
	switch (CAVIAR_DATA_32(chunk_start->tag))
	{
		case CVRCHUNK_FILE_CONTAINER:
			parse_container_chunk(chunk_start, chunk_size, ctxt);
			break;

		case CVRCHUNK_PALETTE_CONTAINER:
			if (ctxt->m_palette != nullptr)
			{
				fprintf(stderr, "Parse error: Palette container within palette container!\n");
				return false;
			}
			ctxt->m_palette = new Palette();
			parse_container_chunk(chunk_start, chunk_size, ctxt);

			// if palette is non-null then store it
			if (ctxt->m_palette)
				ctxt->m_library->palette(ctxt->m_palette->name, ctxt->m_palette);

			ctxt->m_palette = nullptr;
			break;

		case CVRCHUNK_SCENE_CONTAINER:
			parse_container_chunk(chunk_start, chunk_size, ctxt);
			break;

		case CVRCHUNK_OBJECT_CONTAINER:
			if (ctxt->m_model != nullptr)
			{
				fprintf(stderr, "Parse error: Object container within object container!\n");
				return false;
			}
			ctxt->m_model = new Model();
			parse_container_chunk(chunk_start, chunk_size, ctxt);

			// if model is non-null then store it
			if (ctxt->m_model)
				ctxt->m_library->model(ctxt->m_model->name, ctxt->m_model);

			ctxt->m_model = nullptr;
			break;

		case CVRCHUNK_GEOMETRY_CONTAINER:
			parse_container_chunk(chunk_start, chunk_size, ctxt);
			break;

		case CVRCHUNK_ANIMATION_CONTAINER:
			parse_container_chunk(chunk_start, chunk_size, ctxt);
			break;

		case CVRCHUNK_VERSION:
			{
				ChunkVersion * chunk_ver = (ChunkVersion *) chunk_start;
				indent(ctxt);
				printf("Chunk version = %f\n", chunk_ver->version);
			}
			break;

		case CVRCHUNK_DB_NAME:
			{
				ChunkName * chunk_name = (ChunkName *) chunk_start;
				std::string name(chunk_name->name, chunk_size - offsetof(ChunkName, name));
				indent(ctxt);
				printf("DB name = %s\n", name.c_str());
				break;
			}

		case CVRCHUNK_PALETTE_NAME:
			{
				if (ctxt->m_palette == nullptr)
				{
					fprintf(stderr, "Parse error: Palette details outside of palette container!\n");
					return false;
				}
				ChunkName * chunk_name = (ChunkName *) chunk_start;
				ctxt->m_palette->name = std::string(chunk_name->name, chunk_size - offsetof(ChunkName, name));
				ctxt->m_lastPaletteName= ctxt->m_palette->name;
				indent(ctxt);
				printf("Palette name = %s\n", ctxt->m_palette->name.c_str());
				break;
			}


		case CVRCHUNK_PALETTE_DATA:
			{
				if (ctxt->m_palette == nullptr)
				{
					fprintf(stderr, "Parse error: Palette data outside of palette container!\n");
					return false;
				}
				if (ctxt->m_library->isPalette(ctxt->m_palette->name))
				{
					// if palette with this name already exists then skip loading
					delete ctxt->m_palette;
					ctxt->m_palette = nullptr;
					return true;
				}
				indent(ctxt, stderr);
//				fprintf(stderr, "chunk_size = %d\tsizeof(ChunkPaletteData) = %d\n", chunk_size, sizeof(ChunkPaletteData));
				assert(chunk_size >= sizeof(ChunkPaletteData));
				ChunkPaletteData * chunk_palette = (ChunkPaletteData *) chunk_start;

				for (unsigned q = 0; q < 256; ++q)
				{
					ctxt->m_palette->color[q] = ColorRGBA(chunk_palette->color[q].r, chunk_palette->color[q].g, chunk_palette->color[q].b, 255); 
//					printf("Color %02X = [ %d, %d, %d ]\n", q, chunk_palette->color[q].r, chunk_palette->color[q].g, chunk_palette->color[q].b);
				}

				for (unsigned s = 0; s < 24; ++s)
				{
					for (unsigned q = 0; q < 256; ++q)
					{
						ctxt->m_palette->shade[s][q] = chunk_palette->shade[s].s[q];
					}
				}
				break;
			}

		case CVRCHUNK_MATERIAL:
			{
				indent(ctxt);
				if (ctxt->m_palette == nullptr)
				{
					fprintf(stderr, "Parse error: Material data outside palette container!\n");
					return false;
				}
					
				ChunkMaterialData * chunk_material = (ChunkMaterialData *) chunk_start;

				for (unsigned s = 0; s < 24; ++s)
				{
					for (unsigned q = 0; q < 256; ++q)
					{
						uint16_t direct_color = chunk_material->shade[s].c[q];
						ctxt->m_palette->direct_shade[s][q] = ColorRGBA((direct_color & 0xF800) >> 8,
																		(direct_color & 0x07C0) >> 3,
																		(direct_color & 0x003E) << 2,
																		255);
					}
				}
	
				break;
			}

		case CVRCHUNK_SCENE_NAME:
			{
				ChunkName * chunk_name = (ChunkName *) chunk_start;
				std::string name(chunk_name->name, chunk_size - offsetof(ChunkName, name));
				indent(ctxt);
				printf("Scene name = %s\n", name.c_str());
				break;
			}

		case CVRCHUNK_OBJECT_COUNTER:
			{
				ChunkCount * chunk_count = (ChunkCount *) chunk_start;
				indent(ctxt);
				printf("Object count = %d\n", CAVIAR_DATA_32(chunk_count->count));
				break;
			}	

		case CVRCHUNK_FRAME_COUNTER:
			{
				ChunkCount * chunk_count = (ChunkCount *) chunk_start;
				indent(ctxt);
				printf("Frame count = %d\n", CAVIAR_DATA_32(chunk_count->count));
				break;
			}	

		case CVRCHUNK_OBJECT_NAME:
			{
				if (ctxt->m_model == nullptr)
				{
					fprintf(stderr, "Parse error: Object details outside of object container!\n");
					return false;
				}

				ChunkName * chunk_name = (ChunkName *) chunk_start;
				ctxt->m_model->name = std::string(chunk_name->name, chunk_size - offsetof(ChunkName, name));
				indent(ctxt);
				printf("Object name = %s\n", ctxt->m_model->name.c_str());
				break;
			}

		case CVRCHUNK_VOXEL_OBJECT:
			{
				if (ctxt->m_model == nullptr)
				{
					fprintf(stderr, "Parse error: Object data outside of object container!\n");
					return false;
				}
				Caviar::Palette * palette = ctxt->m_library->palette(ctxt->m_lastPaletteName);

				if (palette == nullptr)
				{
					fprintf(stderr, "Internal error: Unknown palette name referenced\n");
					return false;
				}
				VoxelHeader * voxel_hdr = (VoxelHeader *) (chunk_start + 1);
				int32_t voxel_count;
				indent(ctxt);
				printf("Flags:\n");
				voxel_hdr->flags = CAVIAR_DATA_32(voxel_hdr->flags);
				voxel_hdr->voxel_count = CAVIAR_DATA_32(voxel_hdr->voxel_count);
				voxel_hdr->size_total = CAVIAR_DATA_32(voxel_hdr->size_total);

				// FROM NOW ON voxel_hdr content is in host byte order!!
				if (voxel_hdr->flags == 0) {
					indent(ctxt);
					printf(" - (none)\n");
				}
				if (voxel_hdr->flags & 1)
				{
					indent(ctxt);
					printf("- Next voxel group has VoxelHeader\n");
				}
				if (voxel_hdr->flags & 2)
				{
					indent(ctxt);
					printf("- Next voxel group has JumpHeader\n");
				}
				if (voxel_hdr->flags & 4)
				{
					indent(ctxt);
					printf("- Should not be set\n");
				}
				if (voxel_hdr->flags & 8)
				{
					indent(ctxt);
					printf("- Set but unknown\n");
				}
				if (voxel_hdr->flags >> 4)
				{
					indent(ctxt);
					printf("- Some invalid flags set!\n");
				}
//				indent(ctxt);
//				printf("Voxel count = %d\n", voxel_hdr->voxel_count);
				voxel_count = voxel_hdr->voxel_count;
//				indent(ctxt);
//				printf("Apparent voxel object size = %d\n", voxel_hdr->size_total);

				int16_t pos_x = CAVIAR_DATA_16(voxel_hdr->position.x)/* + voxel_hdr->position_offset->x*/;
				int16_t	pos_y = CAVIAR_DATA_16(voxel_hdr->position.y)/* + voxel_hdr->position_offset->y*/;
				int16_t pos_z = CAVIAR_DATA_16(voxel_hdr->position.z)/* + voxel_hdr->position_offset->z*/;

				int32_t voxels_to_read = voxel_hdr->voxel_count;
				
//				indent(ctxt);

//				indent(ctxt, stderr);
//				printf("VoxelHeader = %p\nsizeof(VoxelHeader) = %d\n", (void *) voxel_hdr, sizeof(VoxelHeader));
				VoxelData * voxel_data = nullptr;
				JumpHeader * jump_hdr = nullptr;

				if (voxel_hdr->flags & 2)
				{
					indent(ctxt);
					printf("Instantiating jump header!\n");
					jump_hdr = (JumpHeader *) (voxel_hdr + 1);
				}
				else
				{
//					indent(ctxt);
					voxel_data = (VoxelData *) (voxel_hdr + 1);
				}

				while (voxels_to_read > 0)
				{
					indent(ctxt);
					printf("Reading block containing %d (%d) voxels...\n", voxel_count, voxels_to_read);

					if (jump_hdr != nullptr)
					{
//						indent(ctxt, stderr);
//						printf("Readjusting initial position because jump header exists\n");
						pos_x = jump_hdr->position.x + jump_hdr->position_offset.x;
						pos_y = jump_hdr->position.y + jump_hdr->position_offset.y;
						pos_z = jump_hdr->position.z + jump_hdr->position_offset.z;
						fprintf(stderr, "This voxel group contains %d voxels\n", jump_hdr->count); 
					
//						indent(ctxt, stderr);
//						fprintf(stderr, "Jump header flags = %0X\n", jump_hdr->jump_flags);

						if (jump_hdr->jump_flags & 1)
						{
							voxel_data = (VoxelData *) ((char *) (jump_hdr + 1) + sizeof(uint32_t));
						}
						else
						{
							voxel_data = (VoxelData *) (jump_hdr + 1);
						}
						voxel_count = jump_hdr->count;
					}
/*					else
					{
						pos_x = voxel_hdr->position.x;
						pos_y = voxel_hdr->position.y;
						pos_z = voxel_hdr->position.z;
					}*/
//					printf("Initial position: [%d, %d, %d]\n", pos_x, pos_y, pos_z);

					if (voxel_data == nullptr)
					{
						fprintf(stderr, "Unable to locate voxel data! Bailing out!\n");
						return false;
					}
//					printf("VoxelData = %p (%p)\n", (void *) voxel_data, ((uint64_t) voxel_hdr) + sizeof(VoxelHeader));
					for (unsigned q = 0; q < voxel_count; ++q, --voxels_to_read)
					{
//						indent(ctxt, stderr);
//						fprintf(stderr, "Voxel data = %02X %02X %02X\n", voxel_data->direction, voxel_data->normal, voxel_data->color_idx);
					// insert vertex position into vertex array
//						printf("[%d, %d, %d]\n", pos_x, pos_y, pos_z);
						ctxt->m_model->vertex.push_back(Coords3D(pos_x, pos_y, pos_z));
//						ctxt->m_model->vertex.push_back(Coords3D(pos_x, -pos_y, pos_z));

						// insert vertex RGB color into color array
//						for (int q = 0; q < 2; ++q)
						if (0)
						ctxt->m_model->colors.push_back(
								ColorRGBA(
									255, 
									255, 
									255, 
									255
								)
							);
						else
						{
							unsigned char color = palette->shade[12][voxel_data->color_idx];
							ctxt->m_model->colors.push_back(
									ColorRGBA(
										/*255*/ palette->color[color].x,
										/*255*/ palette->color[color].y, 
										/*255*/ palette->color[color].z,
										255
									)
								);
						}
						// compute normal direction
						uint16_t norm_info = (((uint16_t) voxel_data->direction) & 7) << 8 | ((uint16_t) voxel_data->normal);
						double norm_x, norm_y, norm_z;
						decode_normal(norm_info, &norm_x, &norm_y, &norm_z);

						// insert vertex normal into normal array
//						ctxt->m_model->normal.push_back(Coords3D(norm_z, -norm_x, norm_y));
						ctxt->m_model->normal.push_back(Coords3D(norm_z, norm_x, norm_y));
//						ctxt->m_model->normal.push_back(Coords3D(0, 0, 0));

						// compute next vertex position
						int8_t offsets[4];
						if (relative_position(voxel_data->direction, offsets) != 0)
						{
							indent(ctxt, stderr);
							fprintf(stderr, "Aborting voxel reading at voxel no. %d due to invalid directional vector data 0x%02X!\n", q, voxel_data->direction >> 3);
							exit(1);
						}
	//					printf("[%d, %d, %d] => %d\n", pos_x, pos_y, pos_z, voxel_data->color_idx);
						if (offsets[0] != 1)
						{
							pos_z += offsets[1];
							pos_x += offsets[2];
							pos_y += offsets[3];
						}
						else
						{
//							indent(ctxt);
//							printf("Processed %d voxels\n", q);
							if (jump_hdr && jump_hdr->jump_flags & 1)
							{
								printf("More voxels apparently exist after this block\n");
							}
							voxel_data += 1;
							--voxels_to_read;
							break;
						}
						voxel_data += 1;
					}
//					indent(ctxt, stderr);
//					fprintf(stderr, "Read %d voxels\n", ctxt->m_model->normal.size());
					jump_hdr = (JumpHeader *) (voxel_data);
					//break;
				}
				break;
			}
			
		default:
			indent(ctxt, stderr);
			fprintf(stderr, "I don't know what to do with this chunk. Tag = 0x%08X, size = %d\n", chunk_start->tag, chunk_size);
	}
	ctxt->m_depth--;
	return true;
}

Caviar::Library* Caviar::Library::s_inst = nullptr;
