#pragma once

#include <stdint.h>

/* Internal Caviar format header file.
 * Not intended to be included outside of Caviar library codebase
 */

namespace Caviar {

enum eChunk {
   CVRCHUNK_FILE_CONTAINER         = 0x00525643,
   CVRCHUNK_VERSION                = 0x01000000,
   CVRCHUNK_DB_NAME                = 0x02000000,

   CVRCHUNK_PALETTE_CONTAINER      = 0x03000000,
   CVRCHUNK_PALETTE_NAME           = 0x01010000,
   CVRCHUNK_PALETTE_DATA           = 0x01020000,
   CVRCHUNK_MATERIAL               = 0x01030000,

   CVRCHUNK_SCENE_CONTAINER        = 0x04000000,
   CVRCHUNK_SCENE_NAME             = 0x04010000,
   CVRCHUNK_OBJECT_COUNTER         = 0x04020000,
   CVRCHUNK_FRAME_COUNTER          = 0x04030000,

   CVRCHUNK_OBJECT_CONTAINER       = 0x04040000,
   CVRCHUNK_OBJECT_NAME            = 0x04040100,

   CVRCHUNK_GEOMETRY_CONTAINER     = 0x04040200,
   CVRCHUNK_VOXEL_OBJECT           = 0x04040201,

   CVRCHUNK_ANIMATION_CONTAINER    = 0x04040300,
   CVRCHUNK_OBJECT_FLAG            = 0x04040301,
   CVRCHUNK_OBJECT_LOCATION        = 0x04040302,
   CVRCHUNK_OBJECT_MATRIX          = 0x04040303	
};

/// Generic chunk header
struct ChunkHdr {
	uint32_t	tag;	///< type of chunk content
	uint32_t	size;	///< chunk payload size
//	uint32_t	size_next;
} __attribute__((packed));

/// Chunk version struct
struct ChunkVersion {
	struct ChunkHdr header; ///< chunk header
	float		version;	///< version info
} __attribute__((packed));

/// Chunk containing name of something (e.g. model or palette)
struct ChunkName {
	struct ChunkHdr header;	///< chunk header
	const char name[];		///< string containing name
} __attribute__((packed));

/// Chunk containing count of something (e.g. models or palettes)
struct ChunkCount {
	struct ChunkHdr header;	///< chunk header
	uint32_t count;			///< amount of items in next chunks
} __attribute__((packed));

/// RGB color tuple
struct RGBColor {
	unsigned char r;
	unsigned char g;
	unsigned char b;
} __attribute__((packed));

struct ShadingInfo {
	unsigned char s[256];
} __attribute__((packed));

/// Chunk containing palette data
struct ChunkPaletteData {
	struct ChunkHdr header;  ///< chunk header
	unsigned char first_idx; ///< ID of first valid color in palette
	unsigned char last_idx;  ///< ID of last valic color in palette
	RGBColor color[256];     ///< palette data
	ShadingInfo shade[24];   ///< data related to RGB shading?
} __attribute__((packed));

struct DirectColorInfo {
	uint16_t c[256];
} __attribute__((packed));

struct ChunkMaterialData {
	DirectColorInfo shade[24];
} __attribute__((packed));


/// Vector information
struct ShortVector3 {
	uint16_t	z;
	uint16_t	x;
	uint16_t	y;
} __attribute__((packed));

/// Voxel header struct
struct VoxelHeader {
	uint32_t		flags;
	uint32_t		size_total; // ?
	float			units_per_px;
	uint32_t 		delta_next;
	uint32_t 		group_size;
	ShortVector3 	scale;
	ShortVector3 	position;
	ShortVector3 	pivot;
	uint32_t		voxel_count;
} __attribute__((packed));

/// Voxel header for jump section struct
/// This section is used if it is not possible to trace object surface
/// in one continuous loop but loop has to be interrupted and continue
/// on another place.
struct JumpHeader {
	ShortVector3	position;
	ShortVector3	unknown1;
	ShortVector3	unknown2;
	ShortVector3	position_offset;
	uint32_t		count;
	uint32_t		jump_flags;
} __attribute__((packed));

/// Voxel data struct
struct VoxelData {
	uint8_t			direction;	/// upper bits of direction contain some normal information
	uint8_t			normal;
	uint8_t			color_idx;	///< color index into palette
} __attribute__((packed));



}
