#pragma once

#include <string>
#include <map>
#include <vector>
#include <glm/glm.hpp>
#include <platform_base/types.h>

namespace Caviar {

#if 0
struct Coords3D {
	Coords3D() {}
	Coords3D(float _x, float _y, float _z): x(_x), y(_y), z(_z) {}
	float	x;
	float	y;
	float	z;
};
#endif

typedef glm::vec3	Coords3D;
typedef glm::tvec4<uint8_t> ColorRGBA;

struct Palette {
	Palette() {};
	std::string				name;
	ColorRGBA 					color[256];	
	unsigned char 			shade[24][256];
	ColorRGBA				direct_shade[24][256];
};

struct Model {
	Model() {};
	std::string 			name;
	std::string 			palette_name;
	Coords3D				pivot;

	std::vector<Coords3D>	vertex;
	std::vector<Coords3D>	normal;
	std::vector<ColorRGBA>	colors;
};

typedef std::map<std::string, Caviar::Model *> NameModelMap;
typedef std::map<std::string, Caviar::Palette *> NamePaletteMap;

/** Library holding 3D voxel models.
 * Allows to collect and retrieve models by their names.
 */
struct Library {
	Library() {}
	/** Get instance of library.
	 * @return library singleton
	 */
	static Library * get() { if (s_inst == nullptr) s_inst = new Library(); return s_inst; }
	/** Destroys the Library singleton and everything within.
	 *
	 * Needed for memory debugging/to avoid valgrind/heaptrack noise.
	 */
	static void destroy();

	/** Get model by name.
	 * @param name model name
	 * @return model object OR NULL pointer if model with such name does not exist
	 */
	Model * model(const std::string & name) const;

	/** Store model.
	 * @param name model name (usually from model file)
	 * @param model model object
	 */
	void model(const std::string & name, Model * model);

	const std::vector<Model *> models() const { return m_pModelVector; }

	/** Check if model with given name exists.
	 * @param name model name
	 * @return model existence
	 */
	bool isModel(const std::string & name) const;

	/** Get palette by name.
	 * @param name palette name
	 * @return palette object
	 */
	Palette * palette(const std::string & name) const;

	/** Store palette.
	 * @param name palette name (usually from model file)
	 * @param palette palette object
	 */
	void palette(const std::string & name, Palette * palette);

	/** Check if palette with given name exists.
	 * @param name palette name
	 * @return palette existence
	 */
	bool isPalette(const std::string & name) const;

	NameModelMap	m_model;
	NamePaletteMap	m_palette;
	std::vector<Model *>	m_pModelVector;
	static Library * s_inst;
};

/** Load model file into library.
 * This routine will load palette and geometry from caviar model file,
 * create OpenGL arrays for it and store the model into library. You can
 * access the model later by querying the library.
 * @param f_name file name of caviar model file
 * @return TRUE if model file was processed correctly, FALSE if error occurred
 */
bool open(const std::string & f_name);

}
