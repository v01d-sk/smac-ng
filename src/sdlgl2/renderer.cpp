#include "renderer.h"
#include <platform_base/texturemgr.h>

namespace SMAC_SDLGL2 {

/// Internal vertex type. Represents vertices in GPU memory / shaders.
struct GL2Vertex
{
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec2 texcoord;
	color     col;
};

/** Default shader program. No fancy effects of any kind.
 *
 * Internal to Renderer, not public.
 */
class ProgramDefault
{
public:
	/// Constructs a ProgramDefault, returns nullptr on failure.
	static std::unique_ptr<ProgramDefault> construct();

	/// GL handles of the vertex attributes (vertex data members).
	struct Attributes
	{
		GLint position;
		GLint normal;
		GLint color;
		GLint texcoord;
	};

	/// GL handles of the uniforms (vars set per draw call).
	struct Uniforms
	{
		/// Direction of sunlight, i.e. *from* the sun.
		GLint sun_direction;
		GLint model;
		GLint view;
		GLint projection;

		/// Texture unit ID to use.
		GLint texture_sampler;
	};

	/// Dumb constructor that never fails.
	ProgramDefault(const GLuint program, const Attributes attribs, const Uniforms uniforms)
		: m_program(program)
		, m_attribs(attribs)
		, m_uniforms(uniforms)
	{
	}

	/// Destructor, deletes the internal GL program. 
	~ProgramDefault()
	{
		glDeleteProgram(m_program);
	}

	/// Vertex shader code.
	static inline const char* vshader()
	{
		// TODO texcoords
		return R"glsl(
		#ifdef GL_ES
		precision highp float;
		#endif

		attribute vec4 position;
		attribute vec4 color;
		attribute vec3 normal;
		attribute vec2 texcoord;
		//attribute vec2 texCoord;
		//varying vec2 outTexCord;
		varying vec4 frag_color;
		varying vec3 frag_normal_eyespace;
		varying vec3 frag_position;
		varying vec2 frag_texcoord;
		// TODO for speculars?
		//varying vec4 frag_eye;

		uniform mat4 model;
		uniform mat4 view;
		uniform mat4 projection;

		void main()
		{
			// Smallest point size with no visual gap issues
			gl_PointSize         = 1.4;
			frag_color           = color;
			// TODO use proper normal matrix instead of modelview
			frag_normal_eyespace = normalize(vec3((view * model) * vec4(normal, 0.0)));
			mat4 mvp             = projection * view * model;
			frag_position        = vec3(model * position);
			frag_texcoord        = texcoord;
			gl_Position          = mvp * position;
		}
		)glsl";
	}

	/// Fragment shader code.
	static inline const char* fshader()
	{
		// TODO texcoords, texturing
		return R"glsl(
		#ifdef GL_ES
		precision highp float;
		#endif

		varying vec4 frag_color;
		varying vec3 frag_normal_eyespace;
		varying vec3 frag_position;
		varying vec2 frag_texcoord;
		// TODO for speculars?
		// varying vec4 frag_eye;

		// Values that stay constant for the whole mesh.
		uniform vec3 sun_direction;
		uniform mat4 model;
		uniform mat4 view;
		uniform sampler2D texture_sampler;

		void main()
		{
			// code for a positional light
			// vec3 light_position = vec3(12, 25, 100);
			// vec3 direction_to_light = normalize(light_position - frag_position);

			// TODO settable
			// white, so we see the voxels correctly
			vec3 sun_color = vec3(1, 1, 1.0);
			// vec3 sun_color = vec3(1, 1, 0.6);

			vec3 normal_eyespace = normalize( frag_normal_eyespace );
			// If we have a normal, we calculate lighting. Otherwise we use raw color.
			float norm_len = length(normal_eyespace);

			// only a direction is needed for 'sun' light.
			vec3 direction_to_light_eyespace = normalize( (view * vec4( -sun_direction, 0.0 )) ).xyz;
			// if normal is zero, diffuse_factor is zero.
			float diffuse_factor = max(dot(normal_eyespace, direction_to_light_eyespace), 0.0);

			// if normal is zero, diffuse is zero.
			vec3 diffuse = diffuse_factor * sun_color;
			// TODO settable
			// Bluish dark
			vec3 ambient = vec3(0.1, 0.1, 0.2);

			vec4 texture_color = vec4( 1.0, 1.0, 1.0, 1.0 );
			if( frag_texcoord.x > -256.0 )
			{
				texture_color = texture2D( texture_sampler, frag_texcoord );
			}

			// lighting enabled
			if( norm_len > 0.00001 )
			{
				gl_FragColor = vec4(diffuse + ambient, 1.0) * frag_color * texture_color;
			}
			// lighting disabled
			else
			{
				// TODO test no lighting
				gl_FragColor = frag_color * texture_color;
			}
		}
		)glsl";
	}

	/// Start using this program (and enable its vertex attributes) for subsequent draw calls.
	void use()
	{
		glUseProgram(m_program);
		glEnableVertexAttribArray(m_attribs.position);
		glEnableVertexAttribArray(m_attribs.normal);
		glEnableVertexAttribArray(m_attribs.color);
		glEnableVertexAttribArray(m_attribs.texcoord);
	}

	/// Stop using this program (and disable its vertex attributes).
	void disuse()
	{
		glDisableVertexAttribArray(m_attribs.position);
		glDisableVertexAttribArray(m_attribs.normal);
		glDisableVertexAttribArray(m_attribs.color);
		glDisableVertexAttribArray(m_attribs.texcoord);
		glUseProgram(0);
	}

	/** Set uniforms and attributes for this program.
	 *
	 * Passes the uniforms and sets vertex attrib pointer to currently
	 * bound vertex buffer.
	 *
	 * @param sun_direction        Direction of sunlight, i.e. direction *from* the sun.
	 * @param model                Model->absolute space matrix.
	 * @param view                 absolute space->view space matrix.
	 * @param projection           view space->screen space matrix.
	 *
	 * @note texture sampler is always set to 0 (texture unit 0) and is not exposed here.
	 */
	void set_uniforms_and_attributes(
		const glm::vec3& sun_direction,
		const glm::mat4& model,
		const glm::mat4& view,
		const glm::mat4& projection);

private:
	/// GL shader program handle.
	const GLuint m_program;

	/// GL vertex attribute handles.
	const Attributes m_attribs;

	/// GL vertex uniform handles.
	const Uniforms m_uniforms;
};


std::unique_ptr<ProgramDefault> ProgramDefault::construct()
{
	// Compile vertex/fragment shaders and link them to a program.
	const GLuint program =
		Renderer::build_program("default shader", vshader(), fshader() );
	if(0 == program)
	{
		// result is deleted
		return nullptr;
	}

	// Get handles to uniforms and attributes in the program.
	glUseProgram(program);

	bool missing_attr_uniform = false;
	using Getter = GLint( GLuint, const char* );
	// Loads uniform/attribute with specified name using specified uniform
	// getter function and sets missing_attr_uniform if there is no such
	// uniform/attribute.
	auto get_attr_uniform =
	[&]( Getter function, const char* const name ) {
		const GLint result = function(program, name);
		if(-1 == result)
		{
			fprintf(stderr, "no attribute/uniform '%s' in shader program\n", name);
			missing_attr_uniform = true;
			return -1;
		}
		gl_check_error(name);
		return result;
	};

	ProgramDefault::Attributes attribs;
	ProgramDefault::Uniforms   uniforms;

	// Load uniform/attribute handles.
	attribs.position = get_attr_uniform(glGetAttribLocation, "position");
	attribs.normal   = get_attr_uniform(glGetAttribLocation, "normal");
	attribs.color    = get_attr_uniform(glGetAttribLocation, "color");
	attribs.texcoord = get_attr_uniform(glGetAttribLocation, "texcoord");

	uniforms.sun_direction   = get_attr_uniform(glGetUniformLocation, "sun_direction");
	uniforms.model           = get_attr_uniform(glGetUniformLocation, "model");
	uniforms.view            = get_attr_uniform(glGetUniformLocation, "view");
	uniforms.projection      = get_attr_uniform(glGetUniformLocation, "projection");
	uniforms.texture_sampler = get_attr_uniform(glGetUniformLocation, "texture_sampler");

	if (missing_attr_uniform )
	{
		glDeleteProgram(program);
		return nullptr;
	}

	glUseProgram(0);
	return std::make_unique<ProgramDefault>( program, attribs, uniforms );
}

void ProgramDefault::set_uniforms_and_attributes(
	const glm::vec3& sun_direction,
	const glm::mat4& model,
	const glm::mat4& view,
	const glm::mat4& projection)
{
	// Enable attributes, pointing them to currently bound VBO.
	glVertexAttribPointer(
		m_attribs.position, // attrib handle
		3,                  // number of fields in the attrib
		GL_FLOAT,           // type of fields in the attrib
		GL_FALSE,           // normalize?
		sizeof(GL2Vertex),  // size of the entire vertex
		0);                 // offset of the field in the vertex
	glVertexAttribPointer(
		m_attribs.normal,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(GL2Vertex),
		reinterpret_cast<const void*>(sizeof(glm::vec3)));
	glVertexAttribPointer(
		m_attribs.texcoord,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(GL2Vertex),
		reinterpret_cast<const void*>(2 * sizeof(glm::vec3)));
	glVertexAttribPointer(
		m_attribs.color,
		4,
		GL_UNSIGNED_BYTE,
		GL_TRUE,
		sizeof(GL2Vertex),
		reinterpret_cast<const void*>(2 * sizeof(glm::vec3) + sizeof(glm::vec2)));

	// Upload uniforms.
	glUniform3fv(      m_uniforms.sun_direction,   1, glm::value_ptr(sun_direction));
	glUniformMatrix4fv(m_uniforms.model,           1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(m_uniforms.view,            1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(m_uniforms.projection,      1, GL_FALSE, glm::value_ptr(projection));
	// Use texture unit 0.
	glUniform1i(       m_uniforms.texture_sampler, 0);
	gl_check_error("set_uniforms_and_attributes");
}

std::unique_ptr<Renderer> Renderer::construct(SDL_Window *const window)
{
	fprintf(stderr, "Constructing SDLGL2 renderer\n");

	// Create GL context.
	SDL_GLContext context = SDL_GL_CreateContext(window);
	if (nullptr == context)
	{
		fprintf(stderr, "Failed to create GL context: %s\n", SDL_GetError());
		return nullptr;
	}
	else
	{
		fprintf(stderr, "OK: GL context created\n");
	}
	const unsigned char* const gl_version_string = glGetString(GL_VERSION);
	if (nullptr == gl_version_string)
	{
		fprintf(stderr, "Failed to get GL version string: %s\n", gl_strerr(glGetError()));
		return nullptr;
	}
	printf("GL version: %s\n", gl_version_string);

	// Print the resulting GL screen pixel format. Set up by platform code.
	// (pixel format is specified by SDL platform code).
	int red, green, blue, alpha, depth;
	SDL_GL_GetAttribute(SDL_GL_RED_SIZE,   &red);
	SDL_GL_GetAttribute(SDL_GL_GREEN_SIZE, &green);
	SDL_GL_GetAttribute(SDL_GL_BLUE_SIZE,  &blue);
	SDL_GL_GetAttribute(SDL_GL_ALPHA_SIZE, &alpha);
	SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &depth);
	fprintf(stderr, "Red: %d, Green: %d, Blue: %d, Alpha: %d, Depth: %d\n",
		red, green, blue, alpha, depth);

	if(!gl_check_error("initialization"))
	{
		return nullptr;
	}

	// Set up the index buffer used to draw quads with triangles.
	GLuint ibo;
	constexpr size_t MAX_QUADS = MAX_VERTEX_COUNT / 4;
	uint32_t* const ibo_data = new uint32_t[MAX_QUADS * 6];

	for(uint32_t q = 0; q < MAX_QUADS; ++q)
	{
		const uint32_t voffset = 4 * q;
		const uint32_t ioffset = 6 * q;
		ibo_data[ioffset + 0] = voffset + 0;
		ibo_data[ioffset + 1] = voffset + 1;
		ibo_data[ioffset + 2] = voffset + 2;
		ibo_data[ioffset + 3] = voffset + 0;
		ibo_data[ioffset + 4] = voffset + 2;
		ibo_data[ioffset + 5] = voffset + 3;
	}

	glGenBuffers(1, &ibo);
	gl_check_error("gen index buffer");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_QUADS * 6 * sizeof(uint32_t), ibo_data, GL_STATIC_DRAW);
	gl_check_error("upload index buffer data");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	delete[] ibo_data;

	// Initialize the default GLSL program.
	auto program_default = ProgramDefault::construct();
	if(!program_default)
	{
		return nullptr;
	}

	fprintf(stderr, "Done constructing SDLGL2 renderer\n");
	return std::make_unique<Renderer>(window, context, std::move(program_default), ibo);
}

Renderer::Renderer(
	SDL_Window* const window,
	const SDL_GLContext context,
	std::unique_ptr<class ProgramDefault> program_default,
	const GLuint ibo)
	: m_window(window)
	, m_context(context)
	, m_program_default(std::move(program_default))
	, m_ibo(ibo)
	// Huge allocation (~128MiB), but the OS will only page in what we
	// actually use, which is unlikely to be close to MAX_VERTEX_COUNT
	// (and if it is, we need it).
	, m_scratch_vertex_buffer(new GL2Vertex[MAX_VERTEX_COUNT])
{
	TextureManager::make(*this);
}

Renderer::~Renderer()
{
	TextureManager::erase();
	// Clear and delete the index buffer used to draw quads.
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
	gl_check_error("erase index buffer data");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &m_ibo);
	SDL_GL_DeleteContext(m_context);
	delete[] m_scratch_vertex_buffer;
}

// TODO keep track of frame/noframe state to detect duplicate calls of begin/end frame
void Renderer::begin_frame()
{
	// TODO-ASAP depth buffer for voxels (also in platform.h SDL GL attrib setup)
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Traditional alpha-as-transparency.
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	// Comparison to use for depth test.
	glDepthFunc(GL_LESS);

	const glm::vec2 size = (glm::vec2)window_size();
	glViewport(0, 0, size.x, size.y);
	// TODO-ASAP tweak near/far depths.
	m_projection = glm::ortho(0.0f, size.x, 0.0f, size.y, -400.0f, 400.0f);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	m_program_default->use();
}

void Renderer::end_frame()
{
	m_program_default->disuse();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	gl_check_error("end of frame");
	SDL_GL_SwapWindow(m_window);
}

void Renderer::test_draw()
{
	// Set up a drawing test using the Renderer API.
	glm::vec3 data_position[4];
	data_position[0] = glm::vec3( 5,  40, 1);
	data_position[1] = glm::vec3( 5,  10, 1);
	data_position[2] = glm::vec3( 25, 10, 1);
	data_position[3] = glm::vec3( 25, 40, 1);
	color data_color[4];
	data_color[0] = color(255, 0, 0, 255);
	data_color[1] = color(0, 255, 0, 255);
	data_color[2] = color(0, 0, 255, 255);
	data_color[3] = color(255, 255, 255, 255);
	glm::vec3 data_normal[4];
	data_normal[0] = glm::vec3(0, 0, 1);
	data_normal[1] = glm::vec3(0, 0, 1);
	data_normal[2] = glm::vec3(0, 0, 1);
	data_normal[3] = glm::vec3(0, 0, 1);

	RenderObject object;
	object.vertices = upload_vertex_data(data_position, data_normal, data_color, nullptr, 4);
	object.primitive = PrimitiveType::PointCloud;
	// looking at origin from depth -100 ('behild the camera')
	object.view =
		glm::lookAt( glm::vec3(0, 0, 0),
		             glm::vec3(0, 0, -100),
		             glm::vec3(0, 1, 0) );
	object.translation       = glm::mat4(1.0f);
	object.transformation    = glm::mat4(1.0f);

	draw_object(object);

	delete_vertex_data(object.vertices);
}

void Renderer::test_draw_raw()
{
	// Test drawing with raw GL calls.
	GLuint vbo_position;
	GLuint ibo;

	glm::vec3 data_position[4];
	data_position[0] = glm::vec3(-10,  15, 1);
	data_position[1] = glm::vec3(-10, -15, 1);
	data_position[2] = glm::vec3( 10, -15, 1);
	data_position[3] = glm::vec3( 10,  15, 1);

	// Indices used to emulate quads.
	uint32_t data_indices[6];
	data_indices[0] = 0;
	data_indices[1] = 1;
	data_indices[2] = 2;
	data_indices[3] = 0;
	data_indices[4] = 2;
	data_indices[5] = 3;

	const glm::mat4 view          = glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, 3), glm::vec3(0, 1, 0));
	const glm::vec3 sun_direction = glm::vec3(3, 0, -99);
	const glm::mat4 model         = glm::mat4();

	glGenBuffers(1, &vbo_position);
	gl_check_error("gen vertex buffer");
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data_position), data_position, GL_STATIC_DRAW);
	gl_check_error("upload vertex buffer data");
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &ibo);
	gl_check_error("gen index buffer");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(data_indices), data_indices, GL_STATIC_DRAW);
	gl_check_error("upload index buffer data");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		gl_check_error("bind vertex buffer for drawing");
		m_program_default->set_uniforms_and_attributes(sun_direction, model, view, m_projection);
		// Indices used to emulate quads.
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
	gl_check_error("erase vertex buffer data");
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &vbo_position);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
	gl_check_error("erase index buffer data");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &ibo);
}

VertexData Renderer::upload_vertex_data(
	const glm::vec3* const positions,
	const glm::vec3* const normals,
	const color*     const colors,
	const glm::vec2* const texcoords,
	const size_t           length)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	gl_check_error("gen vertex buffer");
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	if (length > MAX_VERTEX_COUNT)
	{
		fprintf(stderr, "WARNING: too many vertices (%lu) in upload_vertex_data. Cutting to %lu.\n",
		        length, MAX_VERTEX_COUNT);
	}

	const size_t clamped_length = std::min(MAX_VERTEX_COUNT, length);
	// Rearrange vertex attributes to the internal vertex format before uploading.
	for (size_t v = 0; v < clamped_length; ++v)
	{
		GL2Vertex vertex;
		vertex.pos = positions[v];
		// If we don't have any colors on input, fill color data with white.
		// Wastes memory, but simplifies code.
		vertex.col      = colors    ? colors[v]    : color(255, 255, 255, 255);
		vertex.normal   = normals   ? normals[v]   : glm::vec3(0, 0, 0);
		vertex.texcoord = texcoords ? texcoords[v] : glm::vec2(-256, 0);
		m_scratch_vertex_buffer[v] = vertex;
	}
	// Upload vertex data.
	glBufferData(GL_ARRAY_BUFFER, sizeof(GL2Vertex) * clamped_length, m_scratch_vertex_buffer, GL_STATIC_DRAW);

	gl_check_error("upload vertex buffer data");
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	VertexData result;
	// If there is a recyclable instance of GL2VertexData on the freelist,
	// recycle it.
	if (VERTEX_DATA_NULL != m_free_vertex_data)
	{
		result = m_free_vertex_data;
		m_free_vertex_data = m_vertex_buffers[m_free_vertex_data].next;
	}
	// If no VertexData instance to recycle, create a new instance.
	else
	{
		result = m_vertex_buffers.size();
		m_vertex_buffers.push_back( GL2VertexData() );
	}
	m_vertex_buffers[result].vbo    = vbo;
	m_vertex_buffers[result].length = clamped_length;
	// Not on the freelist, so no next item.
	m_vertex_buffers[result].next   = VERTEX_DATA_NULL;

	return result;
}


Texture Renderer::upload_texture(
	const uint8_t *const data,
	const uint16_t       width,
	const uint16_t       height)
{
	const GLuint internal_format = GL_RGBA;
	const GLuint load_format     = internal_format; // must be the same in GLES2
	const GLuint type            = GL_UNSIGNED_BYTE;

	// Generate a texture handle.
	GLuint texture_handle;
	glGenTextures(1, &texture_handle);
	// Bind the texture to upload.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_handle);

	// Nearest is best for 2D assuming we have 1 pixel per pixel
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// Not really important, just for predictability (and maybe we'll use it somewhere).
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// The above should cause an error unless we have a compile-time error, but check anyway.
	gl_check_error("gen texture & initialize texture parameters");

	// Upload the texture.
	glTexImage2D(GL_TEXTURE_2D,
	             0,
	             internal_format,
	             width,
	             height,
	             0,
	             load_format,
	             type,
	             data);
	gl_check_error("upload texture data");
	// TODO in case of error here, use pre-defined dummy texture so e.g.
	//      if texture size is too big, we see what is going on

	// Unbind the texture.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	gl_check_error("unbind texture data");

	TextureData result;
	// If there is a recyclable instance of GL2Texture on the freelist,
	// recycle it.
	if (TEXTURE_DATA_NULL != m_free_texture_data)
	{
		result = m_free_texture_data;
		m_free_texture_data = m_textures[m_free_texture_data].next;
	}
	// If no GL2Texture instance to recycle, create a new instance.
	else
	{
		result = m_textures.size();
		m_textures.push_back( GL2Texture() );
	}

	m_textures[result].texture = texture_handle;
	m_textures[result].width   = width;
	m_textures[result].height  = height;
	m_textures[result].next    = TEXTURE_DATA_NULL;

	return Texture(result, width, height);
}

void Renderer::delete_texture(const Texture& texture)
{
	assert( texture.data < m_textures.size() && "deleting invalid texture" );
	assert( 0 != m_textures[texture.data].texture && "double-deleting texture" );

	const GLuint handle = m_textures[texture.data].texture;
	glDeleteTextures(1, &handle);
	gl_check_error("delete texture name");

	m_textures[texture.data].next    = m_free_texture_data;
	m_textures[texture.data].texture = 0;
	m_free_texture_data              = texture.data;
}

void Renderer::draw_object(const RenderObject& object)
{
	if(TEXTURE_DATA_NULL != object.texture.data)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_textures[object.texture.data].texture);
	}
	// Bind the object's VBO.
	glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffers[object.vertices].vbo);
	gl_check_error("bind vertex buffer for drawing");

	// Set up shader parameters.
	m_program_default->set_uniforms_and_attributes(
		m_sun_direction,
		object.translation * object.transformation,
		object.view,
		m_projection);

	// Draw the object.
	const uint32_t length = m_vertex_buffers[object.vertices].length;
	switch(object.primitive)
	{
		case PrimitiveType::Quads:
			glDrawElements(GL_TRIANGLES, length / 4 * 6, GL_UNSIGNED_INT, 0);
			break;
		case PrimitiveType::PointCloud:
			glDrawArrays(GL_POINTS, 0, length );
			break;
		default:
			assert(false && "Unknown primitive type");
	}

	// Unbind the VBO.
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	if(TEXTURE_DATA_NULL != object.texture.data)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

};
