#include "platform.h"
#include "renderer.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <util/debugprint.h>
#include <caviar/caviar.h>

#include <cstdio>

// TODO Get other GL versions working where we don't have GLSL
using Platform      = SMAC_SDLGL2::Platform;

std::vector<color> generate_striped_texture
	(size_t width,
	 size_t height,
	 size_t stripe_width,
	 color  color_1,
	 color  color_2)
{
	std::vector<color> texture( width * height );
	for( uint32_t y = 0; y < height; ++y )
	{
		for( uint32_t x = 0; x < width; ++x )
		{
			texture[y * width + x] = 
				((y / stripe_width) & 0x1) ? color_1 : color_2;
		}
	}
	return texture;
}

/// Renderer test program.
int main(int argc, char ** argv)
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: %s <model_file>\n", argv[0]);
		return 2;
	}

	Caviar::open( std::string(argv[1]) );

	std::vector<Caviar::Model *> models = Caviar::Library::get()->models();
	std::vector<std::unique_ptr<RenderObject>> objects;

	if (models.empty())
	{
		fprintf(stderr, "Unable to read model %s!\n", argv[1]);
		return 2;
	}

	// Construct Platform, initializing SDL and renderer.

	auto platform = Platform::construct(1024, 768, false);
	if (!platform)
	{
		return 1;
	}

	{
		// Set up a simple quad for testing.
		glm::vec3 data_position[4];
		data_position[0] = glm::vec3( 5,  40, 1);
		data_position[1] = glm::vec3( 5,  10, 1);
		data_position[2] = glm::vec3( 25, 10, 1);
		data_position[3] = glm::vec3( 25, 40, 1);
		color data_color[4];
		data_color[0] = color(255, 255, 255, 255);
		data_color[1] = color(0,  255,  0, 255);
		data_color[2] = color(0,   0, 255, 255);
		data_color[3] = color(255, 255, 255, 255);
		glm::vec3 data_normal[4];
		data_normal[0] = glm::vec3(0, 0, 1);
		data_normal[1] = glm::vec3(0, 0, 1);
		data_normal[2] = glm::vec3(0, 0, 1);
		data_normal[3] = glm::vec3(0, 0, 1);
		glm::vec2 data_texcoord[2];
		data_texcoord[0] = glm::vec2(0, 1);
		data_texcoord[1] = glm::vec2(0, 0);
		data_texcoord[2] = glm::vec2(1, 0);
		data_texcoord[3] = glm::vec2(1, 1);
		// to disable lighting:
		//data_normal[0] = glm::vec3(0, 0, 0);
		//data_normal[1] = glm::vec3(0, 0, 0);
		//data_normal[2] = glm::vec3(0, 0, 0);
		//data_normal[3] = glm::vec3(0, 0, 0);

		// Yellow/Violet
		std::vector<color> test_texture =
			generate_striped_texture( 64, 64, 8, color(255, 255, 0, 255), color(255, 0, 255, 255) );
		Texture texture = platform->renderer().upload_texture( 
			reinterpret_cast<uint8_t*>(test_texture.data()), 64, 64 );

		// Upload the quad to GPU and set up render object transform.
		auto sprite = std::make_unique<RenderObject>();
		sprite->vertices  = platform->renderer().upload_vertex_data(
			data_position,
			data_normal,
			data_color,
			data_texcoord,
			4);
		sprite->primitive = PrimitiveType::Quads;
		sprite->view = 
			glm::lookAt( glm::vec3(0, 0, 0),
			             glm::vec3(0, 0, -100),
			             glm::vec3(0, 1, 0) );
		sprite->translation    = glm::translate( glm::mat4( 1.0f ), glm::vec3(400.0f, 100.0f, 400.0f) );
		sprite->transformation = glm::mat4( 1.0f );
		sprite->texture        = texture;
		objects.push_back(std::move(sprite));
	}

//	fprintf(stderr, "Attempting to add %d vertices.\n", pModel->vertex.size());

	for (Caviar::Model * m : models)
	{
		if (m->vertex.size() == 0)
		{
			continue;
		}
		auto object = std::make_unique<RenderObject>();
		object->vertices  = platform->renderer().upload_vertex_data(
			&m->vertex[0],
			&m->normal[0],
			&m->colors[0],
			nullptr,
			m->vertex.size());
		object->primitive = PrimitiveType::PointCloud;
		object->view = 
			glm::lookAt( glm::vec3(0, 0, 0),
						 glm::vec3(0, 400, -100),
						 glm::vec3(0, 1, 0) );
		object->translation	  = glm::mat4(0.01f);
		object->translation    = glm::rotate(object->translation, (float) ((2.0f*M_PI)/180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		object->translation    = glm::translate(object->translation, glm::vec3(400.0f, 100.0f, 400.0f));
		object->transformation = glm::mat4( 0.2f );

		objects.push_back(std::move(object));
	}

	if (objects.empty())
	{
		printf("Model has no geometry data\n");
		return 0;
	}

	//const glm::vec3 rot_axis( 0, 1, 0 );
	glm::vec4 sun_dir( 0, 0, -100, 0 );

	bool quit = false;
	platform->user_input().add_key_handler(
		[&] (const UserInput& input, KeyState state, Key key) {
			if( state == KeyState::Pressed )
			{
				switch( key )
				{
					case Key::Right: sun_dir = sun_dir * glm::rotate(  0.3f, glm::vec3(0.0f, 1.0f, 0.0f) ); break;
					case Key::Left:  sun_dir = sun_dir * glm::rotate( -0.3f, glm::vec3(0.0f, 1.0f, 0.0f) ); break;
					case Key::Up:    sun_dir = sun_dir * glm::rotate(  0.3f, glm::vec3(1.0f, 0.0f, 0.0f) ); break;
					case Key::Down:  sun_dir = sun_dir * glm::rotate( -0.3f, glm::vec3(1.0f, 0.0f, 0.0f) ); break;
					default: break;
				}
			}

			if( state != KeyState::Released )
			{
				return;
			}
			if( key == Key::K_q && input.is_pressed( Key::LeftCtrl ) )
			{
				quit = true;
			}
		} );
	platform->user_input().add_mouse_move_handler(
		[&] (const UserInput& input, glm::ivec2 pos, glm::ivec2 relative) {
			fprintf(stderr, "mouse move: pos %d %d, rel %d %d\n", 
			        pos.x, pos.y, relative.x, relative.y );
		} );

	auto& renderer = platform->renderer();
	renderer.sun_direction( glm::vec3(sun_dir) );
	// Simple test event loop.
	for( uint64_t frame = 0; ; ++frame )
	{
		// Spam to see that we're not frozen.
		if( 0 == frame % 60 )
		{
			fprintf(stderr, ".");
		}
		if( !platform->handle_events() || quit )
		{
			// Quit e.g. on window close.
			break;
		}
				
		// Set up per-frame renderer parameters.
		renderer.sun_direction( glm::vec3(sun_dir) );
		for (auto& object : objects)
			object->translation = glm::rotate(object->translation, (float) ((1.0f * M_PI) / 180.0f), glm::vec3(1.0f, 0.0f, 1.0f));


		/* print( stderr, rot ); */
		renderer.begin_frame();
		renderer.depth_enable();
		{
			// Do all rendering here.
			for (auto& object : objects)
				renderer.draw_object(*object);
		}
		renderer.depth_disable();
		renderer.end_frame();
	}

	for (auto& object : objects)
		platform->renderer().delete_vertex_data(object->vertices);

	Caviar::Library::destroy();
	return 0;
}


