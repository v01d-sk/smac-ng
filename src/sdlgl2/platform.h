#pragma once

#include "renderer.h"
#include "../platform_base/userinput.h"

#include <SDL2/SDL.h>

#include <memory>
#include <cassert>
#include <cstdint>

namespace SMAC_SDLGL2{

/** SDLGL2 platform (could be refactored/shared with other SDL2 platform implementations).
 *
 * Built using SDL2; the Renderer it constructed is built on top of GL/ES 2.0.
 *
 * Handles platform-specific code such as user input/events, sound, etc. Graphics is handled 
 * separately in a Renderer class - instantiated by Platform::renderer().
 */
class Platform {
public:
	/** Platform construction function.
	 *
	 * All the important construction logic is here; the Platform constructor
	 * just takes already initialized members.
	 *
	 * Initializes SDL2 sets up video mode, window and GL context, and constructs the Renderer.
	 *
	 * @return a valid Platform on success.
	 * @return a null unique_ptr on failure.
	 */
	static std::unique_ptr<Platform> construct( 
		const uint32_t width,
		const uint32_t height,
		const bool     fullscreen)
	{
		fprintf(stderr, "Constructing SDLGL2 platform\n");
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS) < 0)
		{
			fprintf(stderr, "Failed to initialize SDL: %s\n", SDL_GetError());
			return nullptr;
		}

		// RGBA8, no need for anything else until we have 10bit monitors.
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE,     8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,   8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,    8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,   8);
		// Depth buffering needed at least to prevent a single voxel drawing over itself.
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,   24);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	 
		// TODO fall back to plain 2.0 if ES 2.0 does not work
		// Request OpenGL ES 2.0
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
		
		uint32_t window_flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;
		if (fullscreen) {window_flags |= SDL_WINDOW_FULLSCREEN;}
		// Create a window. Will fail if GLES2 is not available
		SDL_Window* const window = SDL_CreateWindow
			("Proxima Centauri", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
			 width, height, window_flags);
		if (nullptr == window)
		{
			fprintf(stderr, "Failed to create window: %s\n", SDL_GetError());
			return nullptr;
		}

		auto renderer = Renderer::construct(window);
		if (!renderer)
		{
			fprintf(stderr, "Failed to initialize renderer.\n");
			return nullptr;
		}
	 
		fprintf(stderr, "Done constructing SDLGL2 platform\n");
		return std::make_unique<Platform>(std::move(renderer), window);
	}

	/** Access the Renderer for graphics operations. */
	Renderer& renderer()
	{
		return *m_renderer;
	}

	/** Access user input for callbacks and polling. */
	UserInput& user_input()
	{
		return m_user_input;
	}

	/** Collects platform events such as keyboard/mouse I/O, window events and so on.
	 *
	 * @return true if the program should continue to run.
	 * @return false if a quit/exit event is caught.
	 */
	bool handle_events()
	{
		SDL_Event event;
		bool done = false;
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
				case SDL_QUIT:
				case SDL_WINDOWEVENT_CLOSE:
					done = true;
					break;
				case SDL_KEYDOWN:
				case SDL_KEYUP:
					m_user_input.emit_key(
						event.type == SDL_KEYUP ? KeyState::Released : KeyState::Pressed,
						static_cast<Key>(event.key.keysym.sym));
					break;
				case SDL_MOUSEBUTTONDOWN:
				case SDL_MOUSEBUTTONUP:
				{
					MouseKey mouse_key;
					switch(event.button.button)
					{
						case SDL_BUTTON_LEFT:   mouse_key = MouseKey::Left;   break;
						case SDL_BUTTON_MIDDLE: mouse_key = MouseKey::Middle; break;
						case SDL_BUTTON_RIGHT:  mouse_key = MouseKey::Right;  break;
					}
					m_user_input.emit_mouse_key(
						event.type == SDL_MOUSEBUTTONUP ? KeyState::Released : KeyState::Pressed,
						mouse_key,
						glm::uvec2(event.button.x, event.button.y));
					break;
				}
				case SDL_MOUSEMOTION:
				{
					auto& motion = event.motion;
					m_user_input.emit_mouse_move(
						glm::ivec2(motion.x,    motion.y),
						glm::ivec2(motion.xrel, motion.yrel) );
					break;
				}
			}

			// TODO handle window resize, etc.
		}

		return !done;
	}

	// public for make_unique
	/** Dumb, no-failure constructor.
	 *
	 * @see construct
	 */
	Platform(std::unique_ptr<Renderer> renderer,
			 SDL_Window* const window)
		: m_renderer(std::move(renderer))
		, m_window(window)
	{}

	/** Destructor.
	 *
	 * Destroys Renderer, window and quits SDL.
	 */ 
	~Platform()
	{
		m_renderer.reset();
		SDL_DestroyWindow(m_window);
		SDL_Quit();
	}

private:
	/** Renderer for graphics operations. Lives as long as the Platform.
	 */
	std::unique_ptr<Renderer> m_renderer;

	UserInput m_user_input;

	/** Owned by Platform, not Renderer, as the window is also used for I/O purposes.
	 */
	SDL_Window* const m_window;
};
};
