#pragma once

/**
 * Overall Renderer design:
 *
 * This is a 'limited low-level' renderer - user can specify vertex data
 * directly, like OpenGL/other graphics APIs, but we don't expose things like
 * shaders and their variables.
 *
 * The renderer should be able to do everything needed for Alpha Centauri
 * while being as simple to implement/maintain as possible. Perfect 
 * performance is not a goal - 'Good enough' is good enough.
 *
 * API features:
 *
 * * Two primitive types: quads (for sprites) and points (for voxels).
 * * Vertex data uploads using simple arrays - returns a VertexData handle
 * * User-constructed RenderObject containing vertex data, matrices for drawing
 * * Lighting - if VertexData has normals, the RenderObject with that VertexData 
 *   is drawn with lighting - otherwise the object will be drawn with raw
 *   colors.
 * * Single adjustable directional (sun) light source.
 * * Separate matrices for projection, view, translation and local
 *   transformation.
 * * Texturing. Works in combination with vertex colors (texture color * vertex color)
 *   Texture coord U below -256 disables texturing.
 * * TODO scissor (rectangle clip) for GUI windows and such.
 *
 * Usage (each frame):
 * * Upload any new vertex data, initialize RenderObjects.
 * * Begin frame.
 * * Draw objects.
 * * End frame.
 *
 *
 * Internal SDLGL2 implementation:
 *
 * Avoid anything that would not work on GL 2.0 hardware. As few state changes
 * as much as possible. This reduces the risk of bugs caused by unexpected API
 * usage or faulty GL implementation.
 *
 * There is only one shader program and only one vertex format. We never rebind
 * the shader - in future we may also use a single internal VBO.
 *
 * Quads are drawn as triangles using indices since GLES2 does not support 
 * quads. There is only one index buffer used for all quad draws - uploaded at
 * startup and never changed. Vertex data are limited to 4M vertices so we
 * don't run out of vertices (see MAX_VERTEX_COUNT).
 *
 * Whether lighting is used depends on vertex content, not a state switch. If
 * vertices of an object have non-zero normals, lighting is calculated.
 * Otherwise vertex/texture color is used directly (as if fully lit)
 *
 * Vertex format is described by struct GL2Vertex.
 *
 * Shader code can be found class ProgramDefault.
 *
 * Texturing only supports one texture per object, and eventually move into a
 * single atlas texture for everything with coords adjusted in shader.
 * 4096x4096 ought to be enough for everyone.
 */

#include <SDL2/SDL.h>
// TODO handle GL/GLES correctly
// #include <GL/gl.h>
// #include <GL/glext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <platform_base/types.h>
#include <util/memory.h>

#include <algorithm>
#include <vector>

#include <memory>
#include <cassert>
#include <cstdint>

namespace SMAC_SDLGL2 {

/// GL error to string for debugging.
static inline const char* gl_strerr(const GLenum error)
{
	switch(error)
	{
		case GL_NO_ERROR:                      return "GL_NO_ERROR";
		case GL_INVALID_ENUM:                  return "GL_INVALID_ENUM";
		case GL_INVALID_VALUE:                 return "GL_INVALID_VALUE";
		case GL_INVALID_OPERATION:             return "GL_INVALID_OPERATION";
		case GL_INVALID_FRAMEBUFFER_OPERATION: return "GL_INVALID_FRAMEBUFFER_OPERATION";
		case GL_OUT_OF_MEMORY:                 return "GL_OUT_OF_MEMORY";
		default:                               return "UNKNOWN_ERROR";
	}
	assert(false);
	return nullptr;
}

/** Checks if previous GL calls caused an error and if so, prints that error to stderr.
 *
 * @param context Context information string, such as the name of the previous GL call.
 */
static inline bool gl_check_error(const char* const context)
{
	const GLenum error = glGetError();
	if (GL_NO_ERROR == error)
	{
		return true;
	}
	fprintf(stderr, "GL error (%s): %s\n", context, gl_strerr(error));
	return false;
}


// Not in Renderer because before C++17 that would need a .cpp declaration, which is ugly.
/** Max number of vertices in a single VertexData for the GL(ES) renderer.
  *
  * Divisible by 4 to simplify quad-based code.
  */
constexpr size_t MAX_VERTEX_COUNT = 4 * 1024 * 1024;

/** GL(ES)2 and SDL2 based renderer.
 */
class Renderer {
private:
	/** Internal vertex data handle.
	 *
	 * Deleted GL2VertexData instances are recycled using a freelist - see
	 * the `next` member.
	 *
	 * Currently GLVertexData maps to a single vertex buffer object; this
	 * may later change with e.g. larger shared VBOs or even a single VBO.
	 *
	 * TODO test the freelist by randomly uploading and deleting vertex data.
	 */
	struct GL2VertexData {
		/// GL vertex buffer storing the vertex data.
		GLuint vbo      = 0;
		/// Number of vertices in the vertex data.
		uint32_t length = 0;
		/** If this GLVertexData instance is deleted, this is the index
		  * of the next deleted GLVertexData.
		  *
		  * The index points into m_vertex_buffers.
		  * m_free_vertex_data points to the first deleted vertex data
		  * instance.
		  *
		  * VERTEX_DATA_NULL (max 32bit value) means there are no further
		  * deleted instances.
		  */
		VertexData next = VERTEX_DATA_NULL;
	};

	/** Internal texture handle.
	 *
	 * Deleted Texture instances are recycled using a freelist - see
	 * the `next` member.
	 *
	 * Currently Texture maps to a single texture object; this may later
	 * change with e.g. an internal texture atlas.
	 *
	 * TODO test the freelist by randomly uploading and deleting textures.
	 */
	struct GL2Texture {
		/// GL texture handle (TODO texture atlas with shader-side texcoord translation).
		GLuint texture = 0;
		/// Width of the texture in pixels.
		uint16_t width = 0;
		/// Height of the texture in pixels.
		uint16_t height = 0;
		/** If this GLTexture instance is deleted, this is the index
		  * of the next deleted GLTexture.
		  *
		  * The index points into m_textures. m_free_texture points to the
		  * first deleted texture instance.
		  *
		  * TEXTURE_DATA_NULL (max 32bit value) means there are no further
		  * deleted instances.
		  */
		TextureData next = TEXTURE_DATA_NULL;
	};


public:
	static_assert( MAX_VERTEX_COUNT % 4 == 0,
	               "MAX_VERTEX_COUNT must be divisible into quads" );

	/** Renderer construction function.
	 *
	 * All the important construction logic is here; the Renderer constructor
	 * just takes already initialized members.
	 *
	 * @param window SDL window to render to (created by Platform).
	 *
	 * @return a valid Renderer on success.
	 * @return a null unique_ptr on failure.
	 */
	static std::unique_ptr<Renderer> construct(SDL_Window *const window);

	/// Dumb constructor that just passes members and never fails.
	Renderer(SDL_Window* const window,
	         const SDL_GLContext context,
	         std::unique_ptr<class ProgramDefault> program_default,
	         const GLuint ibo);

	/// Destroy the renderer, deleting the GL context.
	~Renderer();

	/// Get window size as a (width, height) vector.
	glm::uvec2 window_size()
	{
		int width, height;
		SDL_GetWindowSize(m_window, &width, &height);
		return glm::uvec2(width, height);
	}

	//TODO keep track of frame/noframe state to detect duplicate calls of start/end frame
	/** Begin a frame.
	 *
	 * Sets up GL state and viewport.
	 *
	 * Call before any drawing.
	 */
	void begin_frame();

	/** End a frame and swap buffers.
	 *
	 * Call after all drawing.
	 */
	void end_frame();

	/// Simple function to test that we can in fact draw using Renderer calls.
	void test_draw();

	/// Simple function to test that we can in fact draw using direct GL calls.
	void test_draw_raw();

	/** Upload vertex buffer data.
	 *
	 * @param positions Vertex position data. Must be specified.
	 * @param normals   Normal values at each vertex. These are especially
	 *                  useful for voxel (point cloud) rendering where each
	 *                  point should have a normal.
	 *                  If NULL, the vertex data will always be rendered
	 *                  without lighting (at full color). Normals are
	 *                  expected to be normalized or zero. Zero normals
	 *                  result in vertices/primitives with those normals
	 *                  being drawn without lighting.
	 * @param colors    Color values for each vertex.
	 *                  Vertex color is combined with texture color, if
	 *                  a texture is used (TODO).
	 *                  If NULL, the vertex color is assumed to be white.
	 * @param texcoords Texture coordinate values for each vertex. If NULL,
	 *                  texturing will not be used for objects with this vertex
	 *                  data.
	 * @param length    Number of items in all passed non-NULL arrays.
	 *                  Must not be more than MAX_VERTEX_COUNT (any more
	 *                  vertices will be cut off and ignored.)
	 *
	 * @return Handle to uploaded vertex data, usable with RenderObject.
	 */
	VertexData upload_vertex_data(const glm::vec3* const positions,
	                              const glm::vec3* const normals,
	                              const color*     const colors,
	                              const glm::vec2* const texcoords,
	                              const size_t           length);

	// TODO delete/check all remaining vertex/texture data in dtor.
	/** Delete vertex data indentified by handle data.
	 */
	void delete_vertex_data(const VertexData data)
	{
		assert(data < m_vertex_buffers.size() && "deleting invalid vertex data");
		assert(m_vertex_buffers[data].vbo != 0 && "double-deleting vertex data");

		const GLuint vbo = m_vertex_buffers[data].vbo;
		// Bind the VBO.
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		gl_check_error("bind vertex buffer data for deletion");
		// Clear the VBO/tell the driver that it really is no longer used.
		glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
		gl_check_error("erase vertex buffer data");
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Delete the VBO.
		glDeleteBuffers(1, &vbo);

		// Add to freelist.
		m_vertex_buffers[data].next = m_free_vertex_data;
		m_vertex_buffers[data].vbo  = 0;
		m_free_vertex_data          = data;
	}

	/** Upload texture data and get an (ID+width+height) handle to it.
	 *
	 * @param data   Raw image data in RGBA8 format, as a linear array.
	 * @param width  Width of the texture.
	 * @param height Height of the texture.
	 *
	 * Size of data is calculated as width * height * 4 (because RGBA8).
	 *
	 * @return handle to uploaded texture.
	 */
	Texture upload_texture(const uint8_t *const data,
	                       const uint16_t width,
	                       const uint16_t height);

	/** Delete texture identified by specified handle.
	 */
	void delete_texture(const Texture& texture);

	/** Draw a RenderObject.
          *
	  * @param object Object to draw.
	  *
	  * Position and transformation of the object depends on the matrices
	  * in the RenderObject, as does the primitive used.
	  */
	void draw_object(const RenderObject& object);

	/** Set the sun direction vector.
	 *
	 * TODO sun color
	 * TODO more 'sun' (directional) lights (Alpha Cen B?)
	 * TODO positional lights (for cities, units and the like).
	 */
	void sun_direction( const glm::vec3 direction )
	{
		m_sun_direction = direction;
	}

	/// Enable depth test. Needed for voxel drawing.
	void depth_enable()
	{
		// Enable depth test / depth buffering.
		glEnable(GL_DEPTH_TEST);
	}

	/// Disable depth test. Useful for GUI (and maybe some map?) drawing.
	void depth_disable()
	{
		// Disable depth test / depth buffering.
		glDisable(GL_DEPTH_TEST);
	}

	/////////////////////////////////////////////////////////////////////////////
	// Package internal public functions follow; not part of the Renderer API. //
	/////////////////////////////////////////////////////////////////////////////

	/** Build a simple GLSL program and return a GL handle to it.
	 *
	 * One vertex shader, one fragment shader, nothing more.
	 *
	 * @param name        Name of the program for debugging purposes.
	 * @param vshader_src Vertex shader source.
	 * @param fshader_src Fragment shader source.
	 *
	 * @return ID of the shader program on success.
	 * @return 0 on failure
	 */
	static GLuint build_program( const char* const name,
	                             const char* const vshader_src,
	                             const char* const fshader_src )
	{
		GLuint program;
		GLuint vshader;
		GLuint fshader;
		bool success = true;

		// Print any error that occured during compilation of a shader,
		// and return the (passed) compilation status.
		auto check_compilation = [&](const GLuint shader, const GLint compiled){
			GLint info_bytes;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_bytes);
			// 1 is needed because zero terminator
			if(1 < info_bytes)
			{
				// this is cheap as long as we don't init shaders all the time
				auto info = new char[info_bytes];
				glGetShaderInfoLog(shader, info_bytes, NULL, info);
				fprintf(stderr, "Shader compilation info (%s):\n %s\n", name, info);
				delete[] info;
			}
			return compiled;
		};

		// Create shader objects.
		vshader = glCreateShader(GL_VERTEX_SHADER);
		if( 0 == vshader)
		{
			fprintf( stderr, "vshader glCreateShader failed (%s): %s\n",
			         name, gl_strerr(glGetError()));
			goto error_exit;
		}
		fshader = glCreateShader(GL_FRAGMENT_SHADER);
		if( 0 == fshader)
		{
			fprintf( stderr, "fshader glCreateShader failed (%s): %s\n",
			         name, gl_strerr(glGetError()));
			goto cleanup_vshader;
		}

		// Compile shaders.
		glShaderSource(vshader, 1, &vshader_src, NULL);
		success = success && gl_check_error("vshader source upload");
		glShaderSource(fshader, 1, &fshader_src, NULL);
		success = success && gl_check_error("fshader source upload");
		glCompileShader(vshader);
		success = success && gl_check_error("vshader compilation");
		glCompileShader(fshader);
		success = success && gl_check_error("fshader compilation");

		// Check and handle compilation status.
		GLint compiled;
		glGetShaderiv(vshader, GL_COMPILE_STATUS, &compiled);
		if(!check_compilation(vshader, compiled))
		{
			fprintf( stderr, "Failed to compile vertex shader for '%s'.\n", name);
			goto cleanup_shaders;
		}
		glGetShaderiv(fshader, GL_COMPILE_STATUS, &compiled);
		if(!check_compilation(fshader, compiled))
		{
			fprintf( stderr, "Failed to compile fragment shader for '%s'.\n", name);
			goto cleanup_shaders;
		}

		// Create shader program object.
		program = glCreateProgram();
		if(0 == program)
		{
			fprintf( stderr, "glCreateProgram failed (%s): %s\n", name, gl_strerr(glGetError()));
			goto cleanup_shaders;
		}

		// Attach shaders to the program.
		glAttachShader(program, vshader);
		success = success && gl_check_error("vshader attach");
		glAttachShader(program, fshader);
		success = success && gl_check_error("fshader attach");

		// Link the program
		glLinkProgram(program);

		// Check and handle the link status
		GLint linked;
		glGetProgramiv(program, GL_LINK_STATUS, &linked);
		GLint info_bytes;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_bytes);
		// 1 because zero terminator
		if(1 < info_bytes)
		{
			// this is cheap as long as we don't init shaders all the time
			auto info = new char[info_bytes];
			glGetProgramInfoLog(program, info_bytes, NULL, info);
			fprintf(stderr, "Shader linking info for program '%s':\n %s\n", name, info);
			delete[] info;
		}
		if(!linked)
		{
			fprintf( stderr, "Failed to link shader program '%s'.\n", name);
			goto cleanup_program;
		}

		// Delete shader object (shaders now belong to the program).
		glDeleteShader(vshader);
		success = success && gl_check_error("vshader delete");
		glDeleteShader(fshader);
		success = success && gl_check_error("fshader delete");
		// in this case we got an unhandled error so we are not really in consistent state
		if(!success)
		{
			fprintf(stderr, "Unhandled error/s while building a GLSL shader program");
			goto cleanup_program;
		}

		fprintf(stderr, "Success building shader program '%s'\n", name);

		return program;

cleanup_program:
		glDeleteProgram(program);
cleanup_shaders:
		glDeleteShader(fshader);
cleanup_vshader:
		glDeleteShader(vshader);
error_exit:
		return 0;
	}

private:
	/// SDL window. Owned by platform, not by renderer.
	SDL_Window* const m_window;

	/// GL context. Owned by the renderer.
	const SDL_GLContext m_context;

	/// Default GLSL program. Owned by renderer.
	std::unique_ptr<class ProgramDefault> m_program_default;

	/// Projection matrix.
	glm::mat4 m_projection;

	/// Sun direction vector (direction of the light coming from the sun).
	glm::vec3 m_sun_direction = glm::vec3(3, 0, -99);

	/** Index buffer object used for all quad draws.
	 *
	 * Needed since not all OpenGL contexts support quads.
	 * Contains enough indices for MAX_VERTEX_COUNT vertices.
	 */
	GLuint m_ibo;

	/** Vertices are formed here in correct format (GL2Vertex) before uploading.
	 *
	 * Has MAX_VERTEX_COUNT items.
	 */
	struct GL2Vertex* const m_scratch_vertex_buffer;

	/** Stores GL2VertexData (internal VBO handles + related data).
	 *
	 * Indexed by VertexData.
	 */
	std::vector<GL2VertexData> m_vertex_buffers;

	/** Index of the first instance in the GL2VertexData freelist.
	 *
	 * VERTEX_DATA_NULL means empty list.
	 */
	VertexData m_free_vertex_data = VERTEX_DATA_NULL;

	/** Stores GL2Texture (internal texture handles + related data).
	 *
	 * Indexed by TextureData.
	 */
	std::vector<GL2Texture> m_textures;

	/** Index of the first instance in the GL2Texture freelist.
	 *
	 * TEXTURE_DATA_NULL means empty list.
	 */
	TextureData m_free_texture_data = TEXTURE_DATA_NULL;
};

};
