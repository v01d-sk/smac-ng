#include "pcx.h"
#include <cstdio>
#include "../sdlgl2/platform.h"
#include "../sdlgl2/renderer.h"

#include <util/debugprint.h>
#include <util/dirty.h>
#include <util/resources.h>

#include <smac/map.h>

// TODO Get other GL versions working where we don't have GLSL
using Platform      = SMAC_SDLGL2::Platform;

std::unique_ptr<Platform> platform;

/// Renderer test program.
int main(int argc, char ** argv)
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: %s <smac_dir> <map_file\n\n<smac_dir>    - directory where SMAC data files are installed\n<map_file>   - map file to load, otherwise generic map will be generated\n", argv[0]);
		return 0;
	}

	Resources::init(argv[1]);

/*	std::string textures = std::string(argv[1]) + "/texture.pcx";
	std::string dashboard = std::string(argv[1]) + "/console2_A.pcx";


	Pixmap * p = pcx_load(textures.c_str());

	if (p == nullptr || dash == nullptr)
	{
		return 1;
	}
	
	dash->transparent(100, 16, 156);
*/

	Resources * r = Resources::get();

	if (!r)
	{
		printf("Resource manager not found!\n");
		return 1;
	}

	const Pixmap * dash = r->getPixmap("pix::dash::alien");

	// Construct Platform, initializing SDL and renderer.
	platform = Platform::construct(1024, 768, false);
	if (!platform)
	{
		return 1;
	}

	glm::vec3 * dash_position;
	color * dash_color;
	glm::vec3 * dash_normal;

	long dash_points;
	Map * m = nullptr;
	if (argc < 3)
	{
		m = Map::create(12, 26);
		m->at(0, 0)->altitude(-2100);
		m->at(0, 1)->altitude(-100);
		m->at(1, 0)->altitude(-100);
		m->at(0, 2)->altitude(-100);
		m->at(2, 0)->moist(1);
		m->at(1, 1)->moist(1);
		m->at(1, 2)->moist(1);
		m->at(0, 3)->moist(1);
		m->at(0, 4)->moist(1);
		m->at(0, 0)->vegetation(V_FUNGUS);
		m->at(10, 10)->vegetation(V_FUNGUS);
		m->at(5, 5)->vegetation(V_FOREST);
	}
	else
	{
		m = Map::load(argv[2]);
	}

	dash_points = pixmap_pointcloud(dash, &dash_position, &dash_color, &dash_normal);

	RenderObject * dashobj = new RenderObject();
	dashobj->vertices = platform->renderer().upload_vertex_data(
			dash_position,
			dash_normal,
			dash_color,
			nullptr,
			dash_points);

	free(dash_position);
	free(dash_color);
	free(dash_normal);

	dashobj->primitive = PrimitiveType::PointCloud;
	dashobj->view = glm::lookAt( glm::vec3(0, 0, 0),
								glm::vec3(0, 0, -100),
								glm::vec3(0, 1, 0));
	dashobj->translation = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	dashobj->transformation = glm::mat4(1.0f);

	const glm::vec3 rot_axis( 0, 1, 0 );
	const glm::vec4 sun_start( 0, 0, -100, 0 );
	const float rot_speed = 0.04;

	auto& renderer = platform->renderer();
	renderer.sun_direction( glm::vec3(sun_start) );
	// Simple test event loop.
	//
	int step = 0;
	int dir = 1;
	for( uint64_t frame = 0; ; ++frame )
	{
		// Spam to see that we're not frozen.
		if( 0 == frame % 60 )
		{
			fprintf(stderr, ".");
		}
		if( !platform->handle_events() )
		{
			// Quit e.g. on window close.
			break;
		}
				
		// Set up per-frame renderer parameters.
		glm::mat4 rot = glm::rotate( glm::mat4( 1.0f ), frame * rot_speed, rot_axis );
//		renderer.sun_direction( glm::vec3(rot * sun_start) );

		/* print( stderr, rot ); */
		renderer.begin_frame();
		renderer.depth_enable();
		{
			for (RenderObject * object : m->renderObjects())
			{
			// Do all rendering here.
				renderer.draw_object(*object);
				object->view = glm::lookAt( glm::vec3((float) step * 10, (float) (step * 7), 0.0f),
											glm::vec3((float) step * 10, (float) (step * 7), -100.0f),
											glm::vec3(0, 1, 0));

			}
		}
		renderer.draw_object(*dashobj);
		renderer.depth_disable();
		renderer.end_frame();
		step += dir;
		if (step == 235 || step == -1) 
		{
			dir = -1 * dir;
		}
	}

	for (RenderObject * object : m->renderObjects())
		platform->renderer().delete_vertex_data(object->vertices);
	
	platform->renderer().delete_vertex_data(dashobj->vertices);
	
	delete m;
	Resources::done();
	platform.reset(nullptr);
	printf("\n");
	return 0;

}

