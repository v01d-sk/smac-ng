#include "flc.h"
#include <util/pixmap.h>
#include <third_party/flic/flic.h>

Animation * load_flc(const std::string & name)
{
	FILE * f = fopen(name.c_str(), "rb");
	if (f == nullptr)
		return nullptr;

	flic::StdioFileInterface * fif = new flic::StdioFileInterface(f);

	flic::Decoder * d = new flic::Decoder(fif);
	
	flic::Header header;
	flic::Frame fr_;

	d->readHeader(header);
	fr_.pixels = (uint8_t *) malloc(header.width * header.height * sizeof(uint8_t));
	fr_.rowstride = header.width;

	Animation * out = new Animation(header.width, header.height, header.frames, false);

	for (int q = 0; q < header.frames; ++q)
	{
		d->readFrame(fr_);

		Pixmap * p = new Pixmap(header.width, header.height);

		for (unsigned row = 0; row < p->height; ++row)
			for (unsigned col = 0; col < p->width; ++col)
			{
				uint8_t clr = fr_.pixels[row * fr_.rowstride + col];
				uint32_t off = (row * p->width + col) * 4;
				flic::Color c = fr_.colormap[clr];
				p->buf[off + 0] = c.r;
				p->buf[off + 1] = c.g;
				p->buf[off + 2] = c.b;
				p->buf[off + 3] = 255;
			}

		out->addFrame(q, p);
	}

	free(fr_.pixels);
	delete d;
	delete fif;
	fclose(f);

	return out;
}
