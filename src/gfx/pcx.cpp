#include <stdint.h>
#include <cstdio>
#include <stdlib.h>
#include <string.h>

#include <util/pixmap.h>
#include "pcx.h"

struct PCX_header {
	uint8_t	manuf;
	uint8_t version;
	uint8_t encoding;
	uint8_t bitsPerPlane;
	uint16_t x_min;
	uint16_t y_min;
	uint16_t x_max;
	uint16_t y_max;
	uint16_t v_dpi;
	uint16_t h_dpi;
	uint8_t palette[48];
	uint8_t reserved;
	uint8_t colorPlanes;
	uint16_t bytesPerPlaneLine;
	uint16_t paletteType;
	uint16_t h_scrType;
	uint16_t v_scrType;
	uint8_t pad[54];
} __attribute__((packed));


struct Palette {
	uint8_t r[256];
	uint8_t g[256];
	uint8_t b[256];
};

Pixmap * pcx_load(const char * filename)
{
	Palette pal;
	FILE * f = fopen(filename, "rb");
	if (f == nullptr)
	{
		return nullptr;
	}

	PCX_header header;
	if (fread(&header, sizeof(header), 1, f) != 1)
	{
		fclose(f);
		return nullptr;
	}

	printf("Sizeof PCX_header = %d\n", sizeof(PCX_header));

	if (header.manuf == 0xA)
	{
		printf("Yes, it's a PCX\n");
	}
	else
	{
		fclose(f);
		return nullptr;
	}

	printf("Version = %d\n", header.version);

	if (header.version == 5)
	{
		/* load palette */
		fseek(f, 0, SEEK_END);
		int pos = ftell(f);
		fseek(f, pos - 769, SEEK_SET);
		uint8_t byte;
		fread(&byte, 1, 1, f);
		if (byte != 12)
		{
			printf("This does not seem to be a palette!\n");
			fclose(f);
			return nullptr;
		}

		printf("Loading palette... ");
		for (int q = 0; q < 256; ++q){
			fread(&byte, 1, 1, f);
			pal.r[q] = byte;
			fread(&byte, 1, 1, f);
			pal.g[q] = byte;
			fread(&byte, 1, 1, f);
			pal.b[q] = byte;
		}

		fseek(f, 128, SEEK_SET);
		printf("Done\n");
	}
	
	if (header.encoding == 1)
		printf("Compressed...\n");

	printf("Width = %d\nHeight = %d\n", header.x_max - header.x_min + 1, header.y_max - header.x_min + 1);

	printf("Bits per plane = %d\n", header.bitsPerPlane);

	printf("Color planes = %d\n", header.colorPlanes);

	printf("Bytes per color plane = %d\n", header.bytesPerPlaneLine);

	uint8_t * scanline = (uint8_t *) malloc(header.bytesPerPlaneLine * sizeof(uint8_t));

	Pixmap * p = new Pixmap(header.x_max - header.x_min + 1, header.y_max - header.y_min + 1);

	unsigned cursor = 0;
	unsigned pix_cursor = 0;
	unsigned row = header.y_max - header.y_min + 1;

	while (!feof(f))
	{
		uint8_t byte = 0;
		if (fread(&byte, 1, 1, f) == 1)
		{
			if ((byte & 0xC0) == 0xC0)
			{
				unsigned char count = byte & ~0xC0;
				unsigned char pattern;
				fread(&pattern, 1, 1, f);
//				printf("Writing '%d' %d times\n", pattern, count);
				for (unsigned char q = 0; q < count; ++q)
				{
					scanline[cursor++] = pattern;
				}
			}
			else
			{
//				printf("Writing '%d' once\n", byte);
				scanline[cursor++] = byte;
			}
		}
		if (cursor == header.bytesPerPlaneLine)
		{

			for (unsigned q = 0; q < p->width; ++q)
			{
				p->buf[pix_cursor++] = pal.r[scanline[q]];
				p->buf[pix_cursor++] = pal.g[scanline[q]];
				p->buf[pix_cursor++] = pal.b[scanline[q]];
				p->buf[pix_cursor++] = 255;
			}
			cursor = 0;
			
			if (--row == 0)
				break;
		}
	}

	free(scanline);
	fclose(f);
	return p;
}
