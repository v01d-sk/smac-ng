#include "font.h"
#include <platform_base/types.h>
#include "pixmap.h"
#include <fontconfig/fontconfig.h>

FontProvider::FontProvider(FT_Library library): m_library(library)
{
}

FontProvider::~FontProvider()
{
}

FontProvider * FontProvider::make()
{
	FT_Library lib;
	FT_Error error;
	error = FT_Init_FreeType(&lib);

	if (error == 0)
	{
		return new FontProvider(lib);
	}

	return nullptr;
}

FontProvider * FontProvider::get()
{
	if (s_inst == nullptr)
	{
		s_inst = FontProvider::make();
	}
	assert(s_inst != nullptr && "Unable to initialize freetype!");
	return s_inst;
}

bool FontProvider::loadFontFile(unsigned identifier, const std::string & file_name)
{
	if (m_fonts.find(identifier) != m_fonts.end())
		return false;

	FT_Face font;
	FT_Error error;
	error = FT_New_Face( m_library, file_name.c_str(), 0, &font );/* create face object */

	if (error == 0)
	{
		m_fonts.insert(std::make_pair<>(identifier, new Font(font)));
		return true;
	}

	return false;
}

bool FontProvider::loadFontByName(unsigned identifier, const std::string & font_name)
{
	bool ret = false;
	FcConfig* config = FcInitLoadConfigAndFonts();
	//make pattern from font name
	FcPattern* pat = FcNameParse((const FcChar8*)font_name.c_str());
	FcConfigSubstitute(config, pat, FcMatchPattern);
	FcDefaultSubstitute(pat);
	char* fontFile; //this is what we'd return if this was a function
	// find the font
	FcResult result;
	FcPattern* font = FcFontMatch(config, pat, &result);
	if (font)
	{
		FcChar8* file = NULL;
		if (FcPatternGetString(font, FC_FILE, 0, &file) == FcResultMatch)
		{
			//we found the font, now print it.
			//This might be a fallback font
			fontFile = (char*)file;
			ret = loadFontFile(identifier, fontFile);
		}
	}
	FcPatternDestroy(pat);
	return ret;
}

Font * FontProvider::getFont(unsigned identifier)
{
	if (m_fonts.find(identifier) == m_fonts.end())
		return nullptr;

	return m_fonts.find(identifier)->second;
}

Font::Font(FT_Face face): m_face(face)
{
}

Font::~Font()
{
}

Pixmap * Font::drawText(const std::string & text, color col, unsigned pointSize, unsigned attrs, unsigned maxWidth)
{
	Pixmap * p = new Pixmap(maxWidth, m_face->ascender/64 - m_face->descender/64);
	p->clean();

	FT_Error error;
	FT_Vector pen;
	error = FT_Set_Char_Size( m_face, pointSize * 64, 0, 100, 0 );

	if (error != 0)
		return nullptr;

	FT_GlyphSlot slot = m_face->glyph;
	pen.x = 0;
	pen.y = 0;

	for (unsigned n = 0; n < text.length(); ++n )
	{
		/* set transformation */
		FT_Set_Transform(m_face, nullptr, &pen );

		/* load glyph image into the slot (erase previous one) */
		error = FT_Load_Char(m_face, text[n], FT_LOAD_RENDER );
		if ( error )
		  continue;                 /* ignore errors */

		/* now, draw to our target surface (convert position) */
		draw(p, col, &slot->bitmap,
					 slot->bitmap_left,
					 p->height - (slot->bitmap_top - (m_face->descender/64)) );

		/* increment pen position */
		pen.x += slot->advance.x;
		pen.y += slot->advance.y;
	}

	if (attrs & FONT_ATTR_CENTER)
	{
		unsigned shift = (p->width - (pen.x/64)) / 2;
		for (int r = 0; r < p->height; ++r)
		{
			for (int c = (pen.x/64) + shift; c >= shift; --c)
			{
				unsigned src_offs = (r * p->width + (c - shift)) * 4;
				unsigned dst_offs = (r * p->width + c) * 4;

				p->buf[dst_offs] = p->buf[src_offs];
				p->buf[dst_offs + 1] = p->buf[src_offs + 1];
				p->buf[dst_offs + 2] = p->buf[src_offs + 2];
				p->buf[dst_offs + 3] = p->buf[src_offs + 3];
			}

			for (int c = shift; c >= 0; --c)
			{
				unsigned dst_offs = (r * p->width + c) * 4;
				p->buf[dst_offs] = 0;
				p->buf[dst_offs + 1] = 0;
				p->buf[dst_offs + 2] = 0;
				p->buf[dst_offs + 3] = 0;
			}
		}
	}

	return p;
}

void Font::draw(Pixmap * pix, color col, FT_Bitmap * bitmap, int x, int y)
{
  int  i, j, p, q;
  int  x_max = x + bitmap->width;
  int  y_max = y + bitmap->rows;


  /* for simplicity, we assume that `bitmap->pixel_mode' */
  /* is `FT_PIXEL_MODE_GRAY' (i.e., not a bitmap font)   */

  for ( j = y, q = 0; j < y_max; j++, q++ )
  {
    for ( i = x, p = 0; i < x_max; i++, p++ )
    {
      if ( i < 0      || j < 0       ||
           i >= (int) pix->width || j >= (int)pix->height )
        continue;

	  int offs = (j * pix->width + i) * 4;
	  if (bitmap->buffer[q * bitmap->width + p] == 0)
	  {
		  pix->buf[offs + 0] = 0;
		  pix->buf[offs + 1] = 0;
		  pix->buf[offs + 2] = 0;
	  }
	  else
	  {
	      pix->buf[offs + 0] = col.x;
		  pix->buf[offs + 1] = col.y;
		  pix->buf[offs + 2] = col.z;
		  pix->buf[offs + 3] = bitmap->buffer[q * bitmap->width + p];
	  }
    }
  }
}

FontProvider * FontProvider::s_inst = nullptr;
