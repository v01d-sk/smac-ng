#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

#include <string>
#include <platform_base/types.h>
#include <map>

class Pixmap;
class Font;

typedef std::map<unsigned, Font *> FontDict;

enum FontAttrs { 
	FONT_ATTR_CENTER = 1 << 0 	///< make text centered within renderbox
};

/** Entrypoint to access font loading and accessing facilities.
 * Once instantiated it will initialize FreeType. If required
 * Fontconfig is also initialized and fonts are searched for.
 */
class FontProvider {
protected:
	FontProvider(FT_Library library);
	~FontProvider();

	static FontProvider * make();

public:
	/** Get provider instance.
	 * First call to this will instantiate provider and initialize FreeType.
	 */
	static FontProvider * get();


	/** Load font from file.
	 * @param identifier unique identifier for font. Font can be later obtained by using this identifier value
	 * @param file_name file name pointing to font. Must be case SeNSiTivE on Unix systems!
	 * @return true if font has been loaded, false if not or if identifier is already used
	 */
	bool loadFontFile(unsigned identifier, const std::string & file_name);

	/** Load font by Fontconfig pattern
	 * @note This is portable like Rocky Mountains.
	 * @param identifier unique identifier for font. Font can be later obtained by using this identifier value
	 * @param font_name Freetype pattern used to find closest match for font.
	 * @return true if font has been loaded, false if not or if identifier is already used
	 */
	bool loadFontByName(unsigned identifier, const std::string & font_name);

	/** Get font face by identifier.
	 * @param identifier unique identifier of previously loaded font
	 * @return font object or nullptr if font wasn't loaded using such identifier yet
	 */
	Font * getFont(unsigned identifier);

protected:	
	FT_Library m_library;
	static FontProvider * s_inst;
	FontDict	m_fonts;
};

/** Convenience class for accessing fonts.
 * Represents one TrueType (hopefully) font face and allows to perform
 * some arbitrary operations with it.
 */
class Font {
protected:
	Font(FT_Face face);
	~Font();

public:
	/** Will render given text into pixmap usable for rendering.
	 * @todo Width currently has to be given. Renderer is not yet able to auto-determine box width nor draw multi-line text.
	 * @param text text to render. Non-printable characters will not behave as expected!
	 * @param col color in which text should be rendered. Alpha is ignored for now.
	 * @param pointSize size of font used for rendering
	 * @param attrs rendering attributes (currently only centering is supported)
	 * @param maxWidth maximum rendering width
	 */
	Pixmap * drawText(const std::string & text, color col, unsigned pointSize, unsigned attrs, unsigned maxWidth);

protected:
	void draw(Pixmap * p, color c, FT_Bitmap * bitmap, int x, int y);

protected:
	FT_Face m_face;

	friend class FontProvider;
};
