#include "resources.h"
#include <gfx/pcx.h>
#include "pixmap.h"

Resources::~Resources()
{
	for (auto & res : m_pixmaps)
	{
		delete res.second;
	}
}

void Resources::init(const std::string & root_dir)
{
	Resources::s_inst = new Resources(root_dir);

	Resources * i = Resources::s_inst;
	
	i->loadPCX("pix::terrain::texture", "texture.pcx");
	i->alphaPixmap("pix::terrain::texture", 125, 0, 128);
	
	i->loadPCX("pix::dash::alien", "console2_A.pcx");
	i->alphaPixmap("pix::dash::alien", 100, 16, 156);

	i->loadPCX("pix::sprites::texture", "ter1.pcx");
	i->alphaPixmap("pix::sprites::texture", 152, 24, 228);
	i->alphaPixmap("pix::sprites::texture", 100, 16, 156);

	i->loadPCX("pix::coastline::texture", "Rainfall.pcx");

	i->cropPixmap("pix::terrain::mud", "pix::terrain::texture", 1, 58, 56, 56);
	i->cropPixmap("pix::terrain::grass", "pix::terrain::texture", 1, 115, 56, 56);

	i->cropPixmap("pix::terrain::water_shallow", "pix::terrain::texture", 280, 79, 56, 56);
	i->cropPixmap("pix::terrain::water_deep", "pix::terrain::texture", 280, 136, 56, 56);
	
	i->cropPixmap("pix::terrain::sea_fungus", "pix::terrain::texture", 508, 516, 56, 56);
	i->cropPixmap("pix::terrain::sea_fungus_X", "pix::terrain::texture", 622, 687, 56, 56);
	i->cropPixmap("pix::terrain::sea_fungus_/", "pix::terrain::texture", 508, 573, 56, 56);
	i->cropPixmap("pix::terrain::sea_fungus_\\", "pix::terrain::texture", 508, 573, 56, 56);
	
	i->cropPixmap("pix::terrain::sea_fungus_U", "pix::terrain::texture", 622, 516, 56, 56);
	i->rotatePixmap("pix::terrain::sea_fungus_C", "pix::terrain::sea_fungus_U");
	i->rotatePixmap("pix::terrain::sea_fungus_A", "pix::terrain::sea_fungus_C");
	i->rotatePixmap("pix::terrain::sea_fungus_3", "pix::terrain::sea_fungus_A");


	i->cropPixmap("pix::sprites::energy", "pix::sprites::texture", 1, 379, 100, 62);

	i->cropPixmap("pix::terrain::fungus", "pix::terrain::texture", 280, 516, 56, 56);
	i->cropPixmap("pix::terrain::fungus_X", "pix::terrain::texture", 394, 687, 56, 56);
	i->cropPixmap("pix::terrain::fungus_\\", "pix::terrain::texture", 280, 573, 56, 56);
	i->rotatePixmap("pix::terrain::fungus_/", "pix::terrain::fungus_\\");
	
	i->cropPixmap("pix::terrain::fungus_U", "pix::terrain::texture", 394, 516, 56, 56);
	i->rotatePixmap("pix::terrain::fungus_C", "pix::terrain::fungus_U");
	i->rotatePixmap("pix::terrain::fungus_A", "pix::terrain::fungus_C");
	i->rotatePixmap("pix::terrain::fungus_3", "pix::terrain::fungus_A");

	i->cropPixmap("pix::terrain::forest", "pix::terrain::texture", 526, 6, 56, 56);

	i->cropPixmap("pix::terrain::moist", "pix::terrain::texture", 1, 115, 56, 56);

	i->cropPixmap("pix::terrain::moist_\\", "pix::terrain::texture", 1, 172, 56, 56);
	i->rotatePixmap("pix::terrain::moist_/", "pix::terrain::moist_\\");

	i->cropPixmap("pix::terrain::moist_3", "pix::terrain::texture", 1, 115, 56, 56);
	i->cropPixmap("pix::terrain::moist_U", "pix::terrain::texture", 1, 115, 56, 56);
	i->cropPixmap("pix::terrain::moist_C", "pix::terrain::texture", 1, 115, 56, 56);
	i->cropPixmap("pix::terrain::moist_A", "pix::terrain::texture", 1, 115, 56, 56);

	i->cropPixmap("pix::terrain::moist_X", "pix::terrain::texture", 172, 286, 56, 56);

	i->cropPixmap("pix::terrain::rainy", "pix::terrain::texture", 1, 343, 56, 56);
	
	i->cropPixmap("pix::terrain::rainy_/", "pix::terrain::texture", 1, 400, 56, 56);
	i->cropPixmap("pix::terrain::rainy_\\", "pix::terrain::texture", 1, 400, 56, 56);
	
	i->cropPixmap("pix::terrain::rainy_3", "pix::terrain::texture", 1, 343, 56, 56);
	i->cropPixmap("pix::terrain::rainy_U", "pix::terrain::texture", 1, 343, 56, 56);
	i->cropPixmap("pix::terrain::rainy_C", "pix::terrain::texture", 1, 343, 56, 56);
	i->cropPixmap("pix::terrain::rainy_A", "pix::terrain::texture", 1, 343, 56, 56);
	i->cropPixmap("pix::terrain::rainy_X", "pix::terrain::texture", 172, 514, 56, 56);

	i->cropPixmap("pix::terrain::river", "pix::terrain::texture", 280, 259, 56, 56);
	i->cropPixmap("pix::terrain::river_UR", "pix::terrain::texture", 337, 259, 56, 56);
	i->cropPixmap("pix::terrain::river_DL", "pix::terrain::texture", 280, 316, 56, 56);
	i->cropPixmap("pix::terrain::river_/", "pix::terrain::texture", 337, 316, 56, 56);
	i->cropPixmap("pix::terrain::river_\\", "pix::terrain::texture", 394, 373, 56, 56);

	i->cropPixmap("pix::terrain::river_UL", "pix::terrain::texture", 280, 373, 56, 56);
	i->cropPixmap("pix::terrain::river_DR", "pix::terrain::texture", 394, 259, 56, 56);
	i->cropPixmap("pix::terrain::river_3", "pix::terrain::texture", 280, 430, 56, 56);
	i->cropPixmap("pix::terrain::river_U", "pix::terrain::texture", 337, 373, 56, 56);
	i->cropPixmap("pix::terrain::river_A", "pix::terrain::texture", 394, 316, 56, 56);
	i->cropPixmap("pix::terrain::river_C", "pix::terrain::texture", 451, 259, 56, 56);

	i->cropPixmap("pix::terrain::rocky", "pix::terrain::texture", 1, 1, 56, 56);
	i->cropPixmap("pix::terrain::much_rocky", "pix::terrain::texture", 58, 1, 56, 56);
}

bool Resources::loadPCX(const std::string & resname, const std::string & filename)
{
	std::string fname = m_rootdir + "/" + filename;
	Pixmap * p = pcx_load(fname.c_str());
	if (!p)
		return false;

	m_pixmaps.insert(std::make_pair<>(resname, p));
	return true;
}

bool Resources::alphaPixmap(const std::string & resname, uint8_t r, uint8_t g, uint8_t b)
{
	PixmapList::iterator it = m_pixmaps.find(resname);

	if (it == m_pixmaps.end())
		return false;

	it->second->transparent(r, g, b);

	return true;
}

bool Resources::cropPixmap(const std::string & resname, const std::string & pixmap, unsigned x1, unsigned y1, unsigned w, unsigned h)
{
	PixmapList::const_iterator it = m_pixmaps.find(pixmap);

	if (it == m_pixmaps.end())
		return false;

	Pixmap * p = it->second->crop(x1, y1, x1 + w, y1 + h);

	m_pixmaps.insert(std::make_pair<>(resname, p));

	return true;
}

bool Resources::rotatePixmap(const std::string & resname, const std::string & pixmap)
{
	PixmapList::const_iterator it = m_pixmaps.find(pixmap);

	if (it == m_pixmaps.end())
		return false;

	Pixmap * p = it->second->rotate();

	m_pixmaps.insert(std::make_pair<>(resname, p));

	return true;
}

const Pixmap * Resources::getPixmap(const std::string & resname)
{
	PixmapList::const_iterator it = m_pixmaps.find(resname);

	if (it == m_pixmaps.end())
		return nullptr;

	return it->second;
}

Resources * Resources::s_inst = nullptr;
