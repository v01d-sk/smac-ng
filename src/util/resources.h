#pragma once

#include <stdint.h>
#include <string>
#include <map>

class Pixmap;
class AnimatedPixmap;

typedef std::map<std::string, Pixmap *> PixmapList;
typedef std::map<std::string, AnimatedPixmap *> AnimationList;

class Resources {
protected:
	Resources(const std::string & root_dir): m_rootdir(root_dir) {}
	~Resources();

public:
	/** Initialize resource kit.
     * @param root_dir root directory for finding Resources
	 */
	static void init(const std::string & root_dir);
	static void done() { delete s_inst; s_inst = nullptr; }
	static Resources * get() { return s_inst; }
	bool loadPCX(const std::string & resname, const std::string & filename);
	bool loadFLC(const std::string & resname, const std::string & filename);
	bool alphaPixmap(const std::string & resname, uint8_t r, uint8_t g, uint8_t b);
	bool cropPixmap(const std::string & resname, const std::string & pixmap, unsigned x1, unsigned y1, unsigned w, unsigned h);
	bool rotatePixmap(const std::string & resname, const std::string & pixmap);

	const Pixmap * getPixmap(const std::string & resname);
	const AnimatedPixmap * getAnimation(const std::string & resname);

protected:
	std::string 			m_rootdir;
	PixmapList 				m_pixmaps;
	AnimationList			m_animations;
	static Resources * 		s_inst;
};
