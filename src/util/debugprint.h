#pragma once

#include <glm/glm.hpp>
#include <platform_base/types.h>

#include <cstdio>


/// Print a 4x4 matrix to specified file.
static inline void print( FILE* file, const glm::mat4& m )
{
    fprintf( file, "mat4(%f %f %f %f\n",  m[0].x, m[0].y, m[0].z, m[0].w );
    fprintf( file, "     %f %f %f %f\n",  m[1].x, m[1].y, m[1].z, m[1].w );
    fprintf( file, "     %f %f %f %f\n",  m[2].x, m[2].y, m[2].z, m[2].w );
    fprintf( file, "     %f %f %f %f)\n", m[3].x, m[3].y, m[3].z, m[3].w );
}

/// Print a 3D vector to specified file.
static inline void print( FILE* file, const glm::vec3& v )
{
    fprintf( file, "vec3(%f %f %f)\n", v.x, v.y, v.z );
}

/// Print a 4D vector to specified file.
static inline void print( FILE* file, const glm::vec4& v )
{
    fprintf( file, "vec4(%f %f %f %f)\n", v.x, v.y, v.z, v.w );
}

/// Print an RGBA8 color vector to specified file.
static inline void print( FILE* file, const color& c )
{
    fprintf( file, "color(%u %u %u %u)\n", c.x, c.y, c.z, c.w );
}

/** Print an array of items supported by print() functions to specified file.
 *
 * @param file  File to print to.
 * @param array Array to print.
 * @param size  Number of items in the array.
 *
 * @tparam T Type of items to print (e.g. mat4, vec3).
 */
template <typename T>
static inline void print_array( FILE* file, const T* array, const size_t size )
{
	fprintf( file, "[\n");
	for( size_t i = 0; i < size; ++i )
	{
		print( file, array[i] );
	}
	fprintf( file, "]\n");
}

