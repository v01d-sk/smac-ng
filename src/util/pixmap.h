#pragma once

#include <stdint.h>

struct Pixmap {
	Pixmap(unsigned w, unsigned h);
	~Pixmap();

	Pixmap * crop(unsigned x1, unsigned y1, unsigned x2, unsigned y2) const;
	Pixmap * rotate() const;
	Pixmap * clone() const;
	void transparent(unsigned char r, unsigned char g, unsigned char b);
	void remap(unsigned char r, unsigned char g, unsigned char b, unsigned char o_r, unsigned char o_g, unsigned char o_b, unsigned o_a);
	void blit(const Pixmap * overlay, unsigned x = 0, unsigned y = 0);
	void replace(const Pixmap * source);
	void clean();

	unsigned width, height;
	uint8_t * buf;
};

struct Animation {
	Animation(unsigned w, unsigned h, unsigned fr, bool alloc);
	~Animation();

	Animation * crop(unsigned x1, unsigned y1, unsigned x2, unsigned y2) const;
	Animation * rotate() const;
	Animation * clone() const;

	void transparent(unsigned char r, unsigned char g, unsigned char b);
	void remap(unsigned char r, unsigned char g, unsigned char b, unsigned char o_r, unsigned char o_g, unsigned char o_b, unsigned o_a);
	void blit(const Pixmap * overlay, unsigned x = 0, unsigned y = 0);

	Pixmap * frame(unsigned no) { if (no < frames) return fbuf[no]; return nullptr; }
	bool addFrame(unsigned pos, Pixmap * frame);

	unsigned width, height, frames;

	Pixmap ** fbuf;
};

