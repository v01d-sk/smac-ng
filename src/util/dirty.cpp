#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <util/pixmap.h>
#include <platform_base/types.h>
#include <cstdio>

long pixmap_pointcloud(const Pixmap * p, glm::vec3 ** pos, color ** col, glm::vec3 ** nor)
{
	*pos = (glm::vec3 *) malloc(p->width * p->height * sizeof(glm::vec3));
	*col = (color *) malloc(p->width * p->height * sizeof(color));
	*nor = (glm::vec3 *) malloc(p->width * p->height * sizeof(glm::vec3));
	int cursor = 0;

	for (int r = 0; r < p->height; ++r)
	{
		for (int c = 0; c < p->width; ++c)
		{
			int b_cur = (r * p->width + c) * 4;
			if (p->buf[b_cur + 3] != 0)
			{
				(*pos)[cursor] = glm::vec3(c, p->height - r, 1);
	//			(*col)[cursor] = color(255, 255, 255, 255);
				(*col)[cursor] = color(p->buf[b_cur], p->buf[b_cur + 1], p->buf[b_cur + 2], 255);
				(*nor)[cursor] = glm::vec3(0, 0, 1);
				cursor++;
			}
		}
	}
//	printf("Generated %d points large cloud!\n", cursor);
	return cursor;
}


