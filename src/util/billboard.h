#pragma once

#include <platform_base/types.h>
#include <platform_base/renderer.h>

class Pixmap;
class Animation;

/* Helper class for quick display of bitmap billboards */
class Billboard {
public:
	static Billboard * make(Renderer & renderer, Pixmap * res);
	static Billboard * make(Renderer & renderer, Animation * res);

protected:
	Billboard(Pixmap * res);
	Billboard(Animation * res);
	~Billboard();

	void populate(Renderer & renderer, Pixmap * res);

public:
	bool frame(unsigned no);
	void draw(Renderer & renderer);
	void erase(Renderer & renderer);
	unsigned width() const;
	unsigned height() const; 
	glm::mat4 view() const { return m_renderable.view; }
	void view(const glm::mat4 & v) { m_renderable.view = v; }
	glm::mat4 transformation() const { return m_renderable.transformation; }
	void transformation(const glm::mat4 & t) { m_renderable.transformation = t; }

	Pixmap * pixmap() { return m_pixmap; }

protected:
	Animation * m_animation;
	Pixmap * m_pixmap;
	RenderObject m_renderable;

};
