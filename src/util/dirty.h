#pragma once

long pixmap_pointcloud(const Pixmap * p, glm::vec3 ** pos, color ** col, glm::vec3 ** nor);
