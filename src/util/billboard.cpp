#include "billboard.h"
#include "pixmap.h"
#include <assert.h>
#include <platform_base/texturemgr.h>

/* Static definitions of common billboard properties */

static const color bb_color[4] = { color(255, 255, 255, 255), color(255, 255, 255, 255), color(255, 255, 255, 255), color(255, 255, 255, 255) };
static const glm::vec3 bb_norm[4] = { glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f) };
static const glm::vec2 bb_tex[4] = { glm::vec2(0.0f, 1.0f), glm::vec2(1.0f, 1.0f), glm::vec2(1.0f, 0.0f), glm::vec2(0.0f, 0.0f) };

Billboard::Billboard(Pixmap * res):
	m_animation(nullptr),
	m_pixmap(res)
{
	TextureManager::get()->bind(m_pixmap);
}

Billboard::Billboard(Animation * animation):
	m_animation(animation),
	m_pixmap(animation->frame(0))
{
	for (int q = 0; q < animation->frames; ++q)
	{
		TextureManager::get()->bind(animation->frame(q));
	}
}

void Billboard::populate(Renderer & renderer, Pixmap * res)
{
	m_renderable.primitive = PrimitiveType::Quads;
	m_renderable.translation = glm::mat4(1.0f);
	m_renderable.transformation = glm::mat4(1.0f);
	glm::vec3 bb_pos[4] = { 	glm::vec3(0.0f, 0.0f, 0.0f), 
							glm::vec3((float) res->width, 0.0f, 0.0f), 
							glm::vec3((float) res->width, (float) res->height, 0.0f), 
							glm::vec3(0.0f, (float) res->height, 0.0f) 
						};

	m_renderable.vertices = renderer.upload_vertex_data(bb_pos, bb_norm, bb_color, bb_tex, 4);
	m_renderable.texture = TextureManager::get()->texture(res);
}

void Billboard::erase(Renderer & renderer)
{
//	renderer.delete_vertex_data(m_renderable.vertices);
	m_renderable.vertices = VERTEX_DATA_NULL;

	// unbind animation or pixmap
//	renderer.delete_texture(m_renderable.texture);
	m_renderable.texture.data = TEXTURE_DATA_NULL;

	delete this;
}

Billboard::~Billboard()
{
//	assert(m_renderable.vertices == VERTEX_DATA_NULL && "vertices are not freed!");
//	assert(m_renderable.texture.data == TEXTURE_DATA_NULL && "texture is not freed!");
}

Billboard * Billboard::make(Renderer & renderer, Pixmap * res)
{
	Billboard * bb = nullptr;
	assert(res != nullptr);
	if (res != nullptr)
	{
		bb = new Billboard(res);
		bb->populate(renderer, res);
	}
	return bb;
}

Billboard * Billboard::make(SMAC_SDLGL2::Renderer & renderer, Animation * res)
{
	Billboard * bb = nullptr;
	assert(res != nullptr);
	if (res != nullptr)
	{
		bb = new Billboard(res);
		bb->populate(renderer, res->frame(0));
	}
	return bb;
}

bool Billboard::frame(unsigned no)
{
	if (m_animation == nullptr)
		return false;

	if (no >= m_animation->frames)
		return false;

	Pixmap * frame = m_animation->frame(no);
	m_renderable.texture = TextureManager::get()->texture(frame);
	return true;
}

unsigned Billboard::width() const
{
	return m_pixmap->width;
}

unsigned Billboard::height() const
{
	return m_pixmap->height;
}

void Billboard::draw(Renderer & renderer)
{
	renderer.draw_object(m_renderable);
}
