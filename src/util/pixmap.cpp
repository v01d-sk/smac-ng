#include <string.h>
#include <cstdio>
#include "pixmap.h"

Pixmap::Pixmap(unsigned w, unsigned h)
: width(w), height(h) 
{
	buf = new uint8_t[4 * w * h];
}

Pixmap::~Pixmap()
{
	delete[] buf;
}

Pixmap * Pixmap::crop(unsigned x1, unsigned y1, unsigned x2, unsigned y2) const
{
	Pixmap * op = new Pixmap(x2 - x1, y2 - y1);
	unsigned cursor = 0;
	for (unsigned r = y1; r < y2; ++r)
		for (unsigned c = x1; c < x2; ++c)
		{
			long pos = (r * width + c) * 4;
			for (int q = 0; q < 4; ++q)
				op->buf[cursor++] = buf[pos++];
		}

	return op;
}

Pixmap * Pixmap::clone() const
{
	Pixmap * op = new Pixmap(width, height);
	memcpy(op->buf, buf, 4 * width * height);

	return op;
}

Pixmap * Pixmap::rotate() const
{
	Pixmap * op = new Pixmap(height, width);
	for (unsigned row = 0; row < width; ++row)
		for (unsigned col = 0; col < height; ++col)
		{
			int ipos = (row * width + col) * 4;
			int opos = (col * height + (width - row - 1)) * 4;

			for (int q = 0; q < 4; ++q)
				op->buf[opos + q] = buf[ipos + q];
		}

	return op;
}

void Pixmap::transparent(unsigned char r, unsigned char g, unsigned char b)
{
	remap(r, g, b, 0, 0, 0, 0);
}

void Pixmap::remap(unsigned char r, unsigned char g, unsigned char b, unsigned char o_r, unsigned char o_g, unsigned char o_b, unsigned o_a)
{
	long ct = 0;
	for (unsigned ro = 0; ro < height; ++ro)
	{
		for (unsigned c = 0; c < width; ++c)
		{
			long pos = (ro * width + c) * 4;
			if (buf[pos] == r && buf[pos + 1] == g && buf[pos + 2] == b)
			{
				buf[pos + 0] = o_r;
				buf[pos + 1] = o_g;
				buf[pos + 2] = o_b;
				buf[pos + 3] = o_a;
				ct++;
			}
		}
	}
	printf("Cleared %d pixels\n", ct);
}

void Pixmap::blit(const Pixmap * overlay, unsigned x, unsigned y)
{
	if (overlay == nullptr)
		return;
	if (width < overlay->width + x || height < overlay->height + y)
	{
		fprintf(stderr, "Blitted surface must fall within target surface!\n");
		return;
	}
	
	for (unsigned r = 0; r < overlay->height; ++r)
		for (unsigned c = 0; c < overlay->width; ++c)
		{
			long pos = ((r + y) * width + c + x) * 4;
			long opos = (r * overlay->width + c) * 4;
			if (overlay->buf[opos + 3] != 0)
			{
				int orig_alpha = 255 - overlay->buf[opos + 3];
				int new_alpha = overlay->buf[opos + 3];

				buf[pos] = (orig_alpha * (int) buf[pos] + new_alpha * (int) overlay->buf[opos])/255;
				buf[pos + 1] = (orig_alpha * (int) buf[pos + 1] + new_alpha * (int) overlay->buf[opos + 1])/255;
				buf[pos + 2] = (orig_alpha * (int) buf[pos + 2] + new_alpha * (int) overlay->buf[opos + 2])/255;
				buf[pos + 3] = 255;
			}
		}

}

void Pixmap::replace(const Pixmap * source)
{
	if (width == source->width && height == source->height)
	{
		memcpy(buf, source->buf, width * height * 4);
	}
	else
	{
		fprintf(stderr, "Unable to replace content of bitmap of different size!");
	}
}

void Pixmap::clean()
{
	memset(buf, 0, width * height * 4);
}

Animation::Animation(unsigned w, unsigned h, unsigned fr, bool alloc):
	width(w), height(h), frames(fr)
{
	fbuf = new Pixmap *[fr];

	for (unsigned q = 0; q < fr; ++q)
	{
		if (alloc)
			fbuf[q] = new Pixmap(w, h);
		else
			fbuf[q] = nullptr;
	}
}

Animation::~Animation()
{
	for (unsigned q = 0; q < frames; ++q)
	{
		if (fbuf[q] != nullptr)
			delete fbuf[q];
	}

	delete [] fbuf;
}

Animation * Animation::crop(unsigned x1, unsigned y1, unsigned x2, unsigned y2) const
{
	Animation * out = new Animation(x2 - x1, y2 - y1, frames, false);

	for (unsigned q = 0; q < frames; ++q)
	{
		out->addFrame(q, fbuf[q]->crop(x1, y1, x2, y2));
	}

	return out;
}

Animation * Animation::rotate() const
{
	Animation * out = new Animation(width, height, frames, false);

	for (unsigned q = 0; q < frames; ++q)
	{
		out->addFrame(q, fbuf[q]->rotate());
	}

	return out;

}

Animation * Animation::clone() const
{
	Animation * out = new Animation(width, height, frames, false);

	for (unsigned q = 0; q < frames; ++q)
	{
		out->addFrame(q, fbuf[q]->clone());
	}

	return out;
}

void Animation::transparent(unsigned char r, unsigned char g, unsigned char b)
{
	for (unsigned q = 0; q < frames; ++q)
	{
		fbuf[q]->transparent(r, g, b);
	}
}

void Animation::remap(unsigned char r, unsigned char g, unsigned char b, unsigned char o_r, unsigned char o_g, unsigned char o_b, unsigned o_a)
{
	for (unsigned q = 0; q < frames; ++q)
	{
		fbuf[q]->remap(r, g, b, o_r, o_g, o_b, o_a);
	}
}

void Animation::blit(const Pixmap * overlay, unsigned x, unsigned y)
{
	for (unsigned q = 0; q < frames; ++q)
	{
		fbuf[q]->blit(overlay, x, y);
	}
}

bool Animation::addFrame(unsigned no, Pixmap * frame)
{
	if (frame->width == width && frame->height == height && no < frames)
	{
		fbuf[no] = frame;
		return true;
	}

	return false;
}
