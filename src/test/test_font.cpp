#include <cstdio>
#include <gfx/flc.h>
#include <gfx/pcx.h>
#include <sdlgl2/platform.h>
#include <sdlgl2/renderer.h>
#include <util/pixmap.h>
#include <util/billboard.h>
#include <util/font.h>
#include <platform_base/texturemgr.h>

#include <string>

using Platform      = SMAC_SDLGL2::Platform;

enum FontIdentifiers { FONT_ALPHC, FONT_ARIAL, FONT_ARIAL_BOLD, FONT_ARIAL_ITALIC };

int main(int argc, char ** argv)
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: %s <SMAC_dir>\n\n<SMAC_dir>       - directory containing SMAC files\n", argv[0]);
		return 0;
	}

	Pixmap * background = pcx_load((std::string(argv[1]) + "/openinga.pcx").c_str());
	if (background == nullptr)
	{
		fprintf(stderr, "Unable to open file %s/openinga.pcx!\n", argv[1]);
		return -1;
	}


	Animation * animation = load_flc(std::string(argv[1]) + "/openitem.flc");

	if (animation == nullptr)
	{
		fprintf(stderr, "Unable to open file %s/openitem.flc!\n", argv[1]);
		return -1;
	}
	animation->transparent(255, 0, 255);
	animation->remap(0, 0, 255, 0, 60, 90, 192);

	std::unique_ptr<Platform> platform;
	platform = Platform::construct(1024, 768, false);
	auto& renderer = platform->renderer();
	const glm::vec4 sun_start( 0, 0, -100, 0 );

/*	glm::vec3 flc_position[4];

	flc_position[0] = glm::vec3(0.0f, 0.0f, 0.0f);
	flc_position[1] = glm::vec3((float) animation->width, 0.0f, 0.0f);
	flc_position[2] = glm::vec3((float) animation->width, (float) animation->height, 0.0f);
	flc_position[3] = glm::vec3(0.0f, (float) animation->height, 0.0f);

	color flc_color[4];

	flc_color[0] = color(255, 255, 255, 255);
	flc_color[1] = color(255, 255, 255, 255);
	flc_color[2] = color(255, 255, 255, 255);
	flc_color[3] = color(255, 255, 255, 255);

	glm::vec3 flc_normal[4];

	for (int q = 0; q < 4; ++q)
		flc_normal[q] = glm::vec3(0.0f, 0.0f, 1.0f);

	glm::vec2 flc_texture[4];

	flc_texture[0] = glm::vec2(0.0f, 1.0f);
	flc_texture[1] = glm::vec2(1.0f, 1.0f);
	flc_texture[2] = glm::vec2(1.0f, 0.0f);
	flc_texture[3] = glm::vec2(0.0f, 0.0f);

	int flc_points = 4;*/

	if (!FontProvider::get()->loadFontFile(FONT_ALPHC, std::string(argv[1]) + "/ALPHC___.TTF")
			|| !FontProvider::get()->loadFontByName(FONT_ARIAL, "Arial")
			|| !FontProvider::get()->loadFontByName(FONT_ARIAL_BOLD, "Arial:bold")
			|| !FontProvider::get()->loadFontByName(FONT_ARIAL_ITALIC, "Arial:italic"))
	{
		fprintf(stderr, "Unable to load fonts!");
		return -2;
	}

	Font * alphcFont = FontProvider::get()->getFont(FONT_ARIAL_BOLD);

	Billboard * coords = nullptr;
	Billboard * menu_text[7];

	if (alphcFont)
	{
		Pixmap * p = alphcFont->drawText("(mouse coords)", color(255, 255, 255, 255), 12, 0, 768);
		coords = Billboard::make(platform->renderer(), p);
		coords->view(glm::lookAt(glm::vec3(0, 0, 0),
								glm::vec3(0, 0, -100),
								glm::vec3(0, 1, 0)));
		coords->transformation(glm::translate(coords->transformation(), glm::vec3(20.0f, 20.0f, 0.0f)));

	}

	Billboard * back = Billboard::make(platform->renderer(), background);
	if (back == nullptr)
	{
		fprintf(stderr, "Unable to initialize background billboard!\n");
		return -2;
	}

	back->view(glm::lookAt( glm::vec3(0, 0, 0),
								glm::vec3(0, 0, -100),
								glm::vec3(0, 1, 0)));

	bool animating = true;

	platform->user_input().add_mouse_move_handler(
		[&] (const UserInput& input, glm::ivec2 pos, glm::ivec2 relative) {
			char c_str[20];
			snprintf(c_str, 20, "[%d, %d]", pos.x, pos.y);
			Pixmap * p = alphcFont->drawText(c_str, color(255,255,255,255), 12, 0, 768);
			coords->pixmap()->replace(p);
			TextureManager::get()->update(coords->pixmap());
			delete p;
		}
	);

	platform->user_input().add_mouse_key_handler(
			[&animating] (const UserInput& input, KeyState kstate, MouseKey key, glm::ivec2 relative)
			{
				animating = true;
			}
	);

	Billboard * menu[7];
	const char * menu_label[7] = { "START GAME", "QUICK START", "SCENARIO", "LOAD GAME", "MULTIPLAYER", "VIEW CREDITS", "EXIT GAME" };
	for (int q = 0; q < 7; ++q)
	{
		menu[q] = Billboard::make(platform->renderer(), animation);

		if (menu[q] == nullptr)
		{
			fprintf(stderr, "Unable to initialize menu animation!");
			return -2;
		}
		menu[q]->view(glm::lookAt( glm::vec3(0, 0, 0),
								glm::vec3(0, 0, -100),
								glm::vec3(0, 1, 0)));

		menu[q]->transformation(glm::translate(menu[q]->transformation(), glm::vec3((float) 1024 - menu[q]->width(), (float) q * menu[q]->height(), 0.0f)));
		
		Pixmap * p = alphcFont->drawText(menu_label[q], color(128, 168, 192, 255), 12, FONT_ATTR_CENTER, 286);
		menu_text[q] = Billboard::make(platform->renderer(), p);
		menu_text[q]->view(glm::lookAt(glm::vec3(0, 0, 0),
										glm::vec3(0, 0, -100),
										glm::vec3(0, 1, 0)));
		menu_text[q]->transformation(glm::translate(coords->transformation(), glm::vec3((float) 1024 - menu[q]->width(), (float) q * menu[q]->height(), 0.0f)));
}

	renderer.sun_direction( glm::vec3(sun_start) );
	// Simple test event loop.
	//
	int step = 0;
	int dir = 1;
	uint64_t aframe = 0;

	for( uint64_t frame = 0; ; ++frame )
	{
		if (animating)
		{
			for (int q = 0; q < 7; ++q)
			{
				menu[q]->frame(aframe % animation->frames);
			}
			aframe++;
		}
		if (aframe == animation->frames / 2)
			animating = false;

		if (aframe == animation->frames)
		{
			animating = false;
			aframe = 0;
		}
		
		// Spam to see that we're not frozen.
		if( 0 == frame % 60 )
		{
			fprintf(stderr, ".");
		}
		if( !platform->handle_events() )
		{
			// Quit e.g. on window close.
			break;
		}
				
		// Set up per-frame renderer parameters.
//		glm::mat4 rot = glm::rotate( glm::mat4( 1.0f ), frame * rot_speed, rot_axis );
//		renderer.sun_direction( glm::vec3(rot * sun_start) );

		/* print( stderr, rot ); */
		renderer.begin_frame();
		renderer.depth_disable();
		back->draw(renderer);
		coords->draw(renderer);
		for (int q = 0; q < 7; ++q)
		{
			menu[q]->draw(renderer);
		}
		if (aframe == animation->frames / 2 && animating == false)
		{
			for (int q = 0; q < 7; ++q)
			{
				menu_text[q]->draw(renderer);
			}
		}
		renderer.depth_disable();
		renderer.end_frame();
		step += dir;
		if (step == 235 || step == -1) 
		{
			dir = -1 * dir;
		}
	}

	back->erase(renderer);
	for (int q = 0; q < 6; ++q)
		menu[q]->erase(renderer);

	delete background;
	delete animation;
	platform.reset(nullptr);
	printf("\n");

	return 0;
}
