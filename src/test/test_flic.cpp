#include <cstdio>
#include <gfx/flc.h>
#include <gfx/pcx.h>
#include <sdlgl2/platform.h>
#include <sdlgl2/renderer.h>
#include <util/pixmap.h>
#include <util/billboard.h>

#include <string>

using Platform      = SMAC_SDLGL2::Platform;

int main(int argc, char ** argv)
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: %s <SMAC_dir>\n\n<SMAC_dir>       - directory containing SMAC files\n", argv[0]);
		return 0;
	}

	Pixmap * background = pcx_load((std::string(argv[1]) + "/openinga.pcx").c_str());
	if (background == nullptr)
	{
		fprintf(stderr, "Unable to open file %s/openinga.pcx!\n", argv[1]);
		return -1;
	}


	Animation * animation = load_flc(std::string(argv[1]) + "/openitem.flc");

	if (animation == nullptr)
	{
		fprintf(stderr, "Unable to open file %s/openitem.flc!\n", argv[1]);
		return -1;
	}
	animation->transparent(255, 0, 255);
	animation->remap(0, 0, 255, 0, 60, 90, 192);

	std::unique_ptr<Platform> platform;
	platform = Platform::construct(1024, 768, false);
	auto& renderer = platform->renderer();
	const glm::vec4 sun_start( 0, 0, -100, 0 );

/*	glm::vec3 flc_position[4];

	flc_position[0] = glm::vec3(0.0f, 0.0f, 0.0f);
	flc_position[1] = glm::vec3((float) animation->width, 0.0f, 0.0f);
	flc_position[2] = glm::vec3((float) animation->width, (float) animation->height, 0.0f);
	flc_position[3] = glm::vec3(0.0f, (float) animation->height, 0.0f);

	color flc_color[4];

	flc_color[0] = color(255, 255, 255, 255);
	flc_color[1] = color(255, 255, 255, 255);
	flc_color[2] = color(255, 255, 255, 255);
	flc_color[3] = color(255, 255, 255, 255);

	glm::vec3 flc_normal[4];

	for (int q = 0; q < 4; ++q)
		flc_normal[q] = glm::vec3(0.0f, 0.0f, 1.0f);

	glm::vec2 flc_texture[4];

	flc_texture[0] = glm::vec2(0.0f, 1.0f);
	flc_texture[1] = glm::vec2(1.0f, 1.0f);
	flc_texture[2] = glm::vec2(1.0f, 0.0f);
	flc_texture[3] = glm::vec2(0.0f, 0.0f);

	int flc_points = 4;*/

	Billboard * back = Billboard::make(platform->renderer(), background);
	if (back == nullptr)
	{
		fprintf(stderr, "Unable to initialize background billboard!\n");
		return -2;
	}

	back->view(glm::lookAt( glm::vec3(0, 0, 0),
								glm::vec3(0, 0, -100),
								glm::vec3(0, 1, 0)));

	Billboard * menu[6];
	for (int q = 0; q < 6; ++q)
	{
		menu[q] = Billboard::make(platform->renderer(), animation);

		if (menu[q] == nullptr)
		{
			fprintf(stderr, "Unable to initialize menu animation!");
			return -2;
		}
		menu[q]->view(glm::lookAt( glm::vec3(0, 0, 0),
								glm::vec3(0, 0, -100),
								glm::vec3(0, 1, 0)));

		menu[q]->transformation(glm::translate(menu[q]->transformation(), glm::vec3((float) 1024 - menu[q]->width(), (float) q * menu[q]->height(), 0.0f)));
	}

	renderer.sun_direction( glm::vec3(sun_start) );
	// Simple test event loop.
	//
	int step = 0;
	int dir = 1;

	for( uint64_t frame = 0; ; ++frame )
	{
		for (int q = 0; q < 6; ++q)
		{
			menu[q]->frame((frame / 10) % animation->frames);
		}

		
		// Spam to see that we're not frozen.
		if( 0 == frame % 60 )
		{
			fprintf(stderr, ".");
		}
		if( !platform->handle_events() )
		{
			// Quit e.g. on window close.
			break;
		}
				
		// Set up per-frame renderer parameters.
//		glm::mat4 rot = glm::rotate( glm::mat4( 1.0f ), frame * rot_speed, rot_axis );
//		renderer.sun_direction( glm::vec3(rot * sun_start) );

		/* print( stderr, rot ); */
		renderer.begin_frame();
		renderer.depth_disable();
		back->draw(renderer);
		for (int q = 0; q < 6; ++q)
		{
			menu[q]->draw(renderer);
		}
		renderer.depth_disable();
		renderer.end_frame();
		step += dir;
		if (step == 235 || step == -1) 
		{
			dir = -1 * dir;
		}
	}

	back->erase(renderer);
	for (int q = 0; q < 6; ++q)
		menu[q]->erase(renderer);

	delete background;
	delete animation;
	platform.reset(nullptr);
	printf("\n");

	return 0;
}
