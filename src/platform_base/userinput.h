#pragma once

#include <glm/glm.hpp>
#include <glm/detail/type_vec.hpp>

#include <map>
#include <functional>


/** Key state enum for passing to functions (more readable than bool).
 */
enum class KeyState: uint32_t
{
	Pressed,
	Released
};

/** Keyboard keys; castable to SDL Keysym.
 */
enum class Key: uint32_t
{
	Unknown = SDLK_UNKNOWN,

	Return     = SDLK_RETURN,
	Escape     = SDLK_ESCAPE,
	Backspace  = SDLK_BACKSPACE,
	Tab        = SDLK_TAB,
	Space      = SDLK_SPACE,
	Exclaim    = SDLK_EXCLAIM,
	Quotedbl   = SDLK_QUOTEDBL,
	Hash       = SDLK_HASH,
	Percent    = SDLK_PERCENT,
	Dollar     = SDLK_DOLLAR,
	Ampersand  = SDLK_AMPERSAND,
	Quote      = SDLK_QUOTE,
	LeftParen  = SDLK_LEFTPAREN,
	RightParen = SDLK_RIGHTPAREN,
	Asterisk   = SDLK_ASTERISK,
	Plus       = SDLK_PLUS,
	Comma      = SDLK_COMMA,
	Minus      = SDLK_MINUS,
	Period     = SDLK_PERIOD,
	Slash      = SDLK_SLASH,
	K_0        = SDLK_0,
	K_1        = SDLK_1,
	K_2        = SDLK_2,
	K_3        = SDLK_3,
	K_4        = SDLK_4,
	K_5        = SDLK_5,
	K_6        = SDLK_6,
	K_7        = SDLK_7,
	K_8        = SDLK_8,
	K_9        = SDLK_9,
	Colon      = SDLK_COLON,
	Semicolon  = SDLK_SEMICOLON,
	Less       = SDLK_LESS,
	Equals     = SDLK_EQUALS,
	Greater    = SDLK_GREATER,
	Question   = SDLK_QUESTION,
	At         = SDLK_AT,

	LeftBracket  = SDLK_LEFTBRACKET,
	Backslash    = SDLK_BACKSLASH,
	RightBracket = SDLK_RIGHTBRACKET,
	Caret        = SDLK_CARET,
	Underscore   = SDLK_UNDERSCORE,
	Backquote    = SDLK_BACKQUOTE,

	K_a = SDLK_a,
	K_b = SDLK_b,
	K_c = SDLK_c,
	K_d = SDLK_d,
	K_e = SDLK_e,
	K_f = SDLK_f,
	K_g = SDLK_g,
	K_h = SDLK_h,
	K_i = SDLK_i,
	K_j = SDLK_j,
	K_k = SDLK_k,
	K_l = SDLK_l,
	K_m = SDLK_m,
	K_n = SDLK_n,
	K_o = SDLK_o,
	K_p = SDLK_p,
	K_q = SDLK_q,
	K_r = SDLK_r,
	K_s = SDLK_s,
	K_t = SDLK_t,
	K_u = SDLK_u,
	K_v = SDLK_v,
	K_w = SDLK_w,
	K_x = SDLK_x,
	K_y = SDLK_y,
	K_z = SDLK_z,

	CapsLock = SDLK_CAPSLOCK,

	F1  = SDLK_F1,
	F2  = SDLK_F2,
	F3  = SDLK_F3,
	F4  = SDLK_F4,
	F5  = SDLK_F5,
	F6  = SDLK_F6,
	F7  = SDLK_F7,
	F8  = SDLK_F8,
	F9  = SDLK_F9,
	F10 = SDLK_F10,
	F11 = SDLK_F11,
	F12 = SDLK_F12,

	PrintScreen = SDLK_PRINTSCREEN,
	ScrollLock  = SDLK_SCROLLLOCK,
	Pause       = SDLK_PAUSE,
	Insert      = SDLK_INSERT,
	Home        = SDLK_HOME,
	PageUp      = SDLK_PAGEUP,
	Delete      = SDLK_DELETE,
	End         = SDLK_END,
	PageDown    = SDLK_PAGEDOWN,
	Right       = SDLK_RIGHT,
	Left        = SDLK_LEFT,
	Down        = SDLK_DOWN,
	Up          = SDLK_UP,

	NumlockClear = SDLK_NUMLOCKCLEAR,
	NP_Divide    = SDLK_KP_DIVIDE,
	NP_Multiply  = SDLK_KP_MULTIPLY,
	NP_Minus     = SDLK_KP_MINUS,
	NP_Plus      = SDLK_KP_PLUS,
	NP_Enter     = SDLK_KP_ENTER,

	NP_1 = SDLK_KP_1,
	NP_2 = SDLK_KP_2,
	NP_3 = SDLK_KP_3,
	NP_4 = SDLK_KP_4,
	NP_5 = SDLK_KP_5,
	NP_6 = SDLK_KP_6,
	NP_7 = SDLK_KP_7,
	NP_8 = SDLK_KP_8,
	NP_9 = SDLK_KP_9,
	NP_0 = SDLK_KP_0,

	KP_Period = SDLK_KP_PERIOD,

	Application = SDLK_APPLICATION,
	Power       = SDLK_POWER,
	NP_Equals   = SDLK_KP_EQUALS,

	F13 = SDLK_F13,
	F14 = SDLK_F14,
	F15 = SDLK_F15,
	F16 = SDLK_F16,
	F17 = SDLK_F17,
	F18 = SDLK_F18,
	F19 = SDLK_F19,
	F20 = SDLK_F20,
	F21 = SDLK_F21,
	F22 = SDLK_F22,
	F23 = SDLK_F23,
	F24 = SDLK_F24,

	Execute        = SDLK_EXECUTE,
	Help           = SDLK_HELP,
	Menu           = SDLK_MENU,
	Select         = SDLK_SELECT,
	Stop           = SDLK_STOP,
	Again          = SDLK_AGAIN,
	Undo           = SDLK_UNDO,
	Cut            = SDLK_CUT,
	Copy           = SDLK_COPY,
	Paste          = SDLK_PASTE,
	Find           = SDLK_FIND,
	Mute           = SDLK_MUTE,
	VolumeUp       = SDLK_VOLUMEUP,
	VolumeDown     = SDLK_VOLUMEDOWN,
	KP_Comma       = SDLK_KP_COMMA,
	KP_EqualsAs400 = SDLK_KP_EQUALSAS400,

	AltErase   = SDLK_ALTERASE,
	SysReq     = SDLK_SYSREQ,
	Cancel     = SDLK_CANCEL,
	Clear      = SDLK_CLEAR,
	Prior      = SDLK_PRIOR,
	Return2    = SDLK_RETURN2,
	Separator  = SDLK_SEPARATOR,
	Out        = SDLK_OUT,
	Oper       = SDLK_OPER,
	ClearAgain = SDLK_CLEARAGAIN,
	CrSel      = SDLK_CRSEL,
	ExSel      = SDLK_EXSEL,

	KP_00              = SDLK_KP_00,
	KP_000             = SDLK_KP_000,
	ThousandsSeparator = SDLK_THOUSANDSSEPARATOR,
	DecimalSeparator   = SDLK_DECIMALSEPARATOR,
	CurrencyUnit       = SDLK_CURRENCYUNIT,
	CurrencySubunit    = SDLK_CURRENCYSUBUNIT,
	NP_LeftParen       = SDLK_KP_LEFTPAREN,
	NP_RightParen      = SDLK_KP_RIGHTPAREN,
	NP_LeftBrace       = SDLK_KP_LEFTBRACE,
	NP_RightBrace      = SDLK_KP_RIGHTBRACE,
	NP_Tab             = SDLK_KP_TAB,
	NP_Backspace       = SDLK_KP_BACKSPACE,
	NP_A               = SDLK_KP_A,
	NP_B               = SDLK_KP_B,
	NP_C               = SDLK_KP_C,
	NP_D               = SDLK_KP_D,
	NP_E               = SDLK_KP_E,
	NP_F               = SDLK_KP_F,
	NP_Xor             = SDLK_KP_XOR,
	NP_Power           = SDLK_KP_POWER,
	NP_Percent         = SDLK_KP_PERCENT,
	NP_Less            = SDLK_KP_LESS,
	NP_Greater         = SDLK_KP_GREATER,
	NP_Ampersand       = SDLK_KP_AMPERSAND,
	NP_DblAmpersand    = SDLK_KP_DBLAMPERSAND,
	NP_VerticalBar     = SDLK_KP_VERTICALBAR,
	NP_DblVerticalBar  = SDLK_KP_DBLVERTICALBAR,
	NP_Colon           = SDLK_KP_COLON,
	NP_Hash            = SDLK_KP_HASH,
	NP_Space           = SDLK_KP_SPACE,
	NP_At              = SDLK_KP_AT,
	NP_Exclam          = SDLK_KP_EXCLAM,
	NP_MemStore        = SDLK_KP_MEMSTORE,
	NP_MemRecall       = SDLK_KP_MEMRECALL,
	NP_MemClear        = SDLK_KP_MEMCLEAR,
	NP_MemAdd          = SDLK_KP_MEMADD,
	NP_MemSubtract     = SDLK_KP_MEMSUBTRACT,
	NP_MemMultiply     = SDLK_KP_MEMMULTIPLY,
	NP_MemDivide       = SDLK_KP_MEMDIVIDE,
	NP_PlusMinus       = SDLK_KP_PLUSMINUS,
	NP_Clear           = SDLK_KP_CLEAR,
	NP_ClearEntry      = SDLK_KP_CLEARENTRY,
	NP_Binary          = SDLK_KP_BINARY,
	NP_Octal           = SDLK_KP_OCTAL,
	NP_Decimal         = SDLK_KP_DECIMAL,
	NP_Hexadecimal     = SDLK_KP_HEXADECIMAL,

	LeftCtrl   = SDLK_LCTRL,
	LeftShift  = SDLK_LSHIFT,
	LeftAlt    = SDLK_LALT,
	LeftGUI    = SDLK_LGUI,
	RightCtrl  = SDLK_RCTRL,
	RightShift = SDLK_RSHIFT,
	RightAlt   = SDLK_RALT,
	RightGui   = SDLK_RGUI,

	Mode = SDLK_MODE,

	AudioNext    = SDLK_AUDIONEXT,
	AudioPrev    = SDLK_AUDIOPREV,
	AudioStop    = SDLK_AUDIOSTOP,
	AudioPlay    = SDLK_AUDIOPLAY,
	AudioMute    = SDLK_AUDIOMUTE,
	MediaSelect  = SDLK_MEDIASELECT,
	WWW          = SDLK_WWW,
	Mail         = SDLK_MAIL,
	Calculator   = SDLK_CALCULATOR,
	Computer     = SDLK_COMPUTER,
	AC_Search    = SDLK_AC_SEARCH,
	AC_Home      = SDLK_AC_HOME,
	AC_Back      = SDLK_AC_BACK,
	AC_Forward   = SDLK_AC_FORWARD,
	AC_Stop      = SDLK_AC_STOP,
	AC_Refresh   = SDLK_AC_REFRESH,
	AC_Bookmarks = SDLK_AC_BOOKMARKS,

	BrightnessDown = SDLK_BRIGHTNESSDOWN,
	BrightnessUp   = SDLK_BRIGHTNESSUP,
	DisplaySwitch  = SDLK_DISPLAYSWITCH,
	KbdIllumToggle = SDLK_KBDILLUMTOGGLE,
	KbdIllumDown   = SDLK_KBDILLUMDOWN,
	KbdIllumUp     = SDLK_KBDILLUMUP,
	Eject          = SDLK_EJECT,
	Sleep          = SDLK_SLEEP,
};

/** Most common mouse keys.
 *
 * (TODO back/forward mouse keys?)
 */
enum class MouseKey: uint32_t
{
	Left,
	Middle,
	Right,
	WheelUp,
	WheelDown
};


/** User input dispatcher. Used by / accessible through Platform.
 *
 * Can be used to register callback to handle user input events
 * *as well as* generate input events. 
 *
 * Most of the time, Platform checks for keyboard/mouse input events,
 * and passes them to UserInput. Code using platform registers callback
 * to receive these events.
 *
 * There may eventually be other sources emitting events through UserInput,
 * e.g. game replay records.
 *
 *
 * **Note about keyboard modifiers (Ctrl, Shift, etc.):**
 *
 * Modifiers are not a special type of input - reason: why should I not use some
 * other random key as a modifier (assuming my keyboard doesn't puke from it).
 *
 * Modifier usage example:
 *
 * ---
 * // Platform platform;
 *
 * // Using only LeftCtrl
 * platform.user_input().add_key_handler(
 *     [&] (const UserInput& input, KeyState state, Key key) {
 *         if( key == Key::K_q && state == KeyState::Released &&
 *             input.is_pressed( Key::LeftCtrl ) )
 *         {
 *             // do stuff on Ctrl+Q here (e.g. quit the program)
 *         }
 *     } );
 * // Using either Ctrl key (will refactor this into a function if used often)
 * platform.user_input().add_key_handler(
 *     [&] (const UserInput& input, KeyState state, Key key) {
 *         bool ctrl = input.is_pressed( Key::LeftCtrl ) || input.is_pressed( Key::RightCtrl );
 *         if( key == Key::K_q && state == KeyState::Released && ctrl )
 *         {
 *             // do stuff on Ctrl+Q here (e.g. quit the program)
 *         }
 *     } );
 * ---
 *
 * Callbacks are std::function, which is about the slowest way of doing a
 * callback in C++, but compatible with anything callable and fast enough
 * for this use case.
 */
class UserInput
{
public:
	/** Emit a keyboard key press/release event.
	 *
	 * @param state State of the key event (pressed or released).
	 * @param key   Key that was pressed or released.
	 */
	void emit_key(const KeyState state, const Key key)
	{
		m_pressed_keys[key] = state == KeyState::Pressed ? true : false;
		for(auto& handler: m_key_handlers)
		{
			handler(*this, state, key);
		}
	}

	/** Emit a mouse key press/release event.
	 *
	 * @param state State of the mouse key event (pressed or released).
	 * @param key   Key that was pressed or released.
	 * @param pos   Position of the mouse cursor at the moment of the event.
	 */
	void emit_mouse_key(const KeyState state, const MouseKey key, const glm::ivec2 pos)
	{
		m_pressed_mouse_keys[key] = state == KeyState::Pressed ? true : false;
		// probably unneeded: m_mouse_pos = pos;
		for(auto& handler: m_mouse_key_handlers)
		{
			handler(*this, state, key, pos);
		}
	}

	/** Emit a mouse move event.
	 *
	 * @param pos      Position of the mouse after the mouse move.
	 * @param relative Relative movement of the mouse.
	 */
	void emit_mouse_move(const glm::ivec2 pos, const glm::ivec2 relative)
	{
		m_mouse_pos = pos;
		for(auto& handler: m_mouse_move_handlers)
		{
			handler(*this, pos, relative);
		}
	}

	/** Check if specified keyboard key is currently pressed.
	 */
	bool is_pressed( const Key key ) const
	{
		auto it = m_pressed_keys.find(key);
		// if it's not in the pressed table, it's not pressed.
		return it != m_pressed_keys.end() ? it->second : false;
	}

	/** Check if specified mouse key is currently pressed.
	 */
	bool is_pressed(const MouseKey key) const
	{
		auto it = m_pressed_mouse_keys.find(key);
		// if it's not in the pressed table, it's not pressed.
		return it != m_pressed_mouse_keys.end() ? it->second : false;
	}

	/** Get current mouse position.
	 */
	glm::ivec2 mouse_pos() const
	{
		return m_mouse_pos;
	}

	/** Add a callback that will be called whenever a key is pressed or released.
	 * 
	 * Call multiple times to register multiple callbacks; all added callbacks will be called.
	 *
	 * @param handler A callable (function pointer, lambda, functor, etc.) to
	 *        handle the key event. Must have 3 parameters:
	 *
	 *        - *const UserInput& input*: This input class, for checking status
	 *          of other keys, mouse, etc.
	 *        - *KeyState state*: State of the key (pressed or released).
	 *        - *Key key*: Key that was pressed/released.
	 *
	 * Example:
	 * ---
	 * // Platform platform;
	 * platform.user_input().add_key_handler(
	 *     [&] (const UserInput& input, KeyState state, Key key) {
	 *         if( key   == Key::K_q && 
	 *             state == KeyState::Released &&
	 *             input.is_pressed( Key::LeftCtrl ) )
	 *         {
	 *             // do stuff on Ctrl+Q here (e.g. quit the program)
	 *         }
	 *     } );
	 * ---
	 */
	void add_key_handler(const std::function<void(const UserInput&, KeyState, Key)>& handler)
	{
		m_key_handlers.push_back( handler );
	}

	/** Add a callback that will be called whenever a mouse key is pressed or released.
	 *
	 * Call multiple times to register multiple callbacks; all added callbacks will be called.
	 *
	 * @param handler A callable (function pointer, lambda, functor, etc.) to
	 *        handle the mouse key event. Must have 4 parameters:
	 *
	 *        - *const UserInput& input*: This input class, for checking status
	 *          of other keys, keyboard, etc.
	 *        - *KeyState state*: State of the key (pressed or released).
	 *        - *MouseKey key*: Mouse key that was pressed/released.
	 *        - *glm::ivec2 pos*: Position of the mouse at the time of the press/release.
	 *
	 * Example:
	 *
	 * ---
	 * // Platform platform;
	 * platform.user_input().add_mouse_key_handler(
	 *     [&] (const UserInput& input, KeyState state, MouseKey key, glm::ivec2 pos) {
	 *         if( key   == MouseKey::Right && 
	 *             state == KeyState::Released &&
	 *             input.pressed( Key::LeftCtrl ) )
	 *         {
	 *             // do stuff on Ctrl+RightClick here (e.g. add to selection)
	 *         }
	 *     } );
	 * ---
	 */
	void add_mouse_key_handler(const std::function<void(const UserInput&, KeyState, MouseKey, glm::ivec2)>& handler)
	{
		m_mouse_key_handlers.push_back( handler );
	}

	/** Add a callback that will be called whenever the mouse is moved.
	 *
	 * Call multiple times to register multiple callbacks; all added callbacks will be called.
	 *
	 * @param handler A callable (function pointer, lambda, functor, etc.) to
	 *        handle the mouse move event. Must have 3 parameters:
	 *
	 *        - *const UserInput& input*: This input class, for checking status
	 *          of mouse keys, keyboard, etc.
	 *        - *glm::ivec2 pos*: Position of the mouse after the mouse move.
	 *        - *glm::ivec2 movement*: Change in mouse position since last mouse move event.
	 *
	 * Example:
	 *
	 * ---
	 * // Platform platform;
	 * platform.user_input().add_mouse_move_handler(
	 *     [&] (const UserInput& input, glm::ivec2 pos, glm::ivec2 movement) {
	 *         // update a selection rectangle in an RTS
	 *         if( active_selection_rect )
	 *         {
	 *             update_selection_rect( pos, movement );
	 *         }
	 *     } );
	 * ---
	 */
	void add_mouse_move_handler(const std::function<void(const UserInput&, glm::ivec2, glm::ivec2)>& handler)
	{
		m_mouse_move_handlers.push_back( handler );
	}

private:
	/// Callbacks handling key presses/releases.
	std::vector<std::function<void(const UserInput&, KeyState,   Key)>>                  m_key_handlers;
	/// Callbacks handling mouse key presses/releases.
	std::vector<std::function<void(const UserInput&, KeyState,   MouseKey, glm::ivec2)>> m_mouse_key_handlers;
	/// Callbacks handling mouse movements.
	std::vector<std::function<void(const UserInput&, glm::ivec2, glm::ivec2)>>           m_mouse_move_handlers;

	// Using std::map here is ugly as fuck but works (Key values don't necessarily fit in a
	// low range so can't use a simple array).
	/** Table of key statuses - true is pressed, false or not present in the table is released.
	 */
	std::map<Key, bool>      m_pressed_keys;
	/** Table of mouse key statuses - true is pressed, false or not present in the table is released.
	 */
	std::map<MouseKey, bool> m_pressed_mouse_keys;
	/** Current mouse position.
	 */
	glm::ivec2               m_mouse_pos = glm::ivec2(0, 0);

};
