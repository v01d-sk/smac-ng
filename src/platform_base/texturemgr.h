#pragma once

#include "renderer.h"
#include "types.h"
#include <map>

class Pixmap;

typedef std::map<const Pixmap *, Texture> TextureDict;

class TextureManager {
public:
	static TextureManager * get();

	void bind(const Pixmap * res);
	Texture texture(const Pixmap * res);

	void unbind(const Pixmap * res);

	void update(const Pixmap * res);

	static void make(Renderer & renderer);
	static void erase();

protected:
	TextureManager(Renderer & renderer);

protected:
	static TextureManager * 	s_inst;
	Renderer &					m_renderer;
	TextureDict					m_textures;

//	friend class Renderer;
};

