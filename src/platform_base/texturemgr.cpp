#include "renderer.h"
#include "texturemgr.h"
#include "types.h"
#include <util/pixmap.h>

TextureManager::TextureManager(Renderer & renderer):
	m_renderer(renderer)
{
}

void TextureManager::make(Renderer & renderer)
{
	TextureManager::s_inst = new TextureManager(renderer);
}

void TextureManager::erase()
{
	delete TextureManager::s_inst;
	TextureManager::s_inst = nullptr;
}

TextureManager * TextureManager::get()
{
	assert(TextureManager::s_inst != nullptr && "attempt to use texture manager before when renderer is not (yet/already) initialized!");
	return TextureManager::s_inst;
}

TextureManager * TextureManager::s_inst = nullptr;

void TextureManager::bind(const Pixmap * res)
{
	Texture t;
	TextureDict::const_iterator it = m_textures.find(res);

	if (it == m_textures.end())
	{
		t = m_renderer.upload_texture(res->buf, res->width, res->height);
		m_textures.insert(std::make_pair(res, t));
	}

	return;
}

void TextureManager::unbind(const Pixmap * res)
{
//	abort();
	Texture t;
	TextureDict::iterator it = m_textures.find(res);
	if (it != m_textures.end())
	{
		// texture might never be bound?
		t = it->second;
		m_textures.erase(it);
		m_renderer.delete_texture(t);
	}
}

void TextureManager::update(const Pixmap * res)
{
	unbind(res);
	bind(res);
}

Texture TextureManager::texture(const Pixmap * res)
{
	TextureDict::const_iterator it = m_textures.find(res);
	if (it != m_textures.end())
	{
		return it->second;
	}
	return Texture(TEXTURE_DATA_NULL, res->width, res->height);
}
