#pragma once
/// Platform-independent types used in Platform/Renderer APIs.

#include <glm/glm.hpp>

/// RGBA8 color.
using color = glm::tvec4<uint8_t>;

/// Vertex data handle.
using VertexData = uint32_t;

/// NULL value for VertexData.
constexpr uint32_t VERTEX_DATA_NULL = 0xFFFFFFFF;

/// Texture handle.
using TextureData = uint32_t;

/// NULL value for Texture.
constexpr uint32_t TEXTURE_DATA_NULL = 0xFFFFFFFF;

/// Always RGBA8 (GLES2 only supports RGBA).
struct Texture
{
	/// Texture data handle.
	TextureData data   = TEXTURE_DATA_NULL;
	/// Texture width in pixels.
	uint16_t    width  = 0x0;
	/// Texture height in pixels.
	uint16_t    height = 0x0;

	/// Default constructor for null textures.
	Texture() {}

	/// Constructor from Texture members.
	Texture(const TextureData data, const uint16_t width, const uint16_t height)
		: data(data)
		, width(width)
		, height(height)
	{}
};
static_assert( sizeof(Texture) == 8, "Keep struct Texture small so it can be copied" );

/// Enumerates rendering primitive types.
enum class PrimitiveType: uint8_t
{
	/// Quads. Every 4 vertices form a quad. I.e. 0-3, 4-7, etc.
	Quads,
	/// Each vertex is drawn separately as a point.
	PointCloud
};

/** Renderable 2D/3D object.
 *
 * Points to vertex data managed by a Renderer and describes how to draw
 * and transform that data.
 */
struct RenderObject
{
	/// Reference to vertex data of the object.
	VertexData    vertices;
	/// Texture, if any.
	Texture       texture;
	/// Primitive type goverding how the vertex data is rendered.
	PrimitiveType primitive;

	// zoom as well as camera translation
	glm::mat4     view;
	// TODO replace with a vec3?
	// just translation on the map
	// (separate from zoom/view so we can build model = translation * transformation,
	// which is needed for lighting.
	glm::mat4     translation;
	/// Also known as the Model matrix.
	glm::mat4     transformation;
};
