#pragma once

#include <string>
#include <vector>
#include <map>

namespace SMAC {

typedef std::vector<std::string> StringArray;
typedef std::map<std::string, StringArray> TextData;

bool loadTextFile(const std::string & file, TextData & output);

}
