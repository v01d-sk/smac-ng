#include "rules.h"

int main(int argc, char ** argv)
{
	if (argc > 1)
	{
		SMAC::loadRules(argv[1]);
	}
	else
	{
		fprintf(stderr, "Usage %s <rules.txt>\n", argv[0]);
		return 0;
	}
}
