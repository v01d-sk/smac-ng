#include "rules.h"
#include <string>
#include <string.h>
#include <sstream>
#include <iostream>

using namespace SMAC;

std::string strip(const std::string & in)
{
	std::string output = in;
	while (output[0] == ' ')
		output = output.substr(1);

	while (output[output.length() - 1] == ' ')
		output = output.substr(0, output.length() - 1);

	return output;
}

StringArray strip_split(const std::string & str)
{
	StringArray output;
	std::istringstream f(str);
	std::string s;    
    while (getline(f, s, ',')) {
//        cout << s << endl;
        output.push_back(strip(s));
    }

	return output;
}

SMAC::Technology::Technology(const StringArray & data):
	m_name(data[0]), m_key(data[1])
{
}

bool SMAC::loadRules(const std::string & file)
{
	TextData alpha;

	loadTextFile(file, alpha);

	for (const std::string & technology : alpha["TECHNOLOGY"])
	{
		StringArray sa = strip_split(technology);
		SMAC::Technology * tech = new Technology(sa);
		delete tech;
	}

	return true;
}

