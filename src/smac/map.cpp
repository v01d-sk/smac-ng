#include "map.h"
#include <sdlgl2/platform.h>
#include <sdlgl2/renderer.h>
#include <util/dirty.h>
#include <util/resources.h>
#include <util/pixmap.h>

struct MapLabel {
	uint32_t		x;
	uint32_t		y;
	uint8_t			label[32];
} __attribute__((packed));

struct MapHeader {
	char			sign[10];
	uint8_t			unknown1[5];
	uint32_t		width;
	uint32_t		height;
	uint8_t			unknown2[43-23];
	uint8_t			flag_flat;
	uint8_t			unknown3[3];
	uint32_t		label_count;
	MapLabel		labels[64];
	uint8_t			unknown4[2739 - 2611];
} __attribute__((packed));

struct MapSquare {
	uint8_t			terrain_type;
	uint8_t			altitude;
	uint8_t			unknown1[2];
	uint8_t			zero;
	uint8_t			terrain_flags1;
	uint8_t			unknown2[2];
	uint32_t		terrain_flags2;
	uint16_t		terrain_special;
	uint8_t			unknown3[2];
	uint32_t		unknown4[7];
} __attribute__((packed));

const uint8_t SQUARE_MOIST			= 0x08;
const uint8_t SQUARE_RAINY			= 0x10;

enum Altitude { OCEAN_TRENCH = 0, OCEAN, OCEAN_SHELF, HEIGHT_0, HEIGHT_1, HEIGHT_2, HEIGHT_3, HEIGHT_4 };

using Platform      = SMAC_SDLGL2::Platform;

#define BOREHOLE		0x01000000
#define POD				0x10000000
#define SENSOR			0x80000000
#define AIRBASE			0x00040000
#define FOREST			0x00200000
#define CONDENSER		0x00400000
#define MIRROR			0x00800000
#define SPRING			0x00000100
#define NUTRIENTS		0x02000400
#define ENERGY			0x02010400
#define MINERALS		0x02020400
#define BUNKER			0x00000800
#define MONOLITH		0x00002000
#define FARM			0x00008000
#define ENRICHER		0x00088000
#define ROAD			0x00000004
#define TUBE			0x0000000C
#define MINE			0x00000010
#define FUNGUS			0x00000020
#define SOLAR			0x00000040
#define RIVER			0x00000080

#define UP_RIGHT		0x1
#define RIGHT			0x2
#define DOWN_RIGHT		0x4
#define DOWN			0x8
#define DOWN_LEFT		0x10
#define LEFT			0x20
#define UP_LEFT			0x40
#define UP				0x80

#define DIAG_MAIN		(UP_LEFT | DOWN_RIGHT)
#define DIAG_SECOND		(UP_RIGHT | DOWN_LEFT)

extern std::unique_ptr<Platform> platform;

const char * analyze_shape(uint8_t shape)
{
	const char * rt = "";
	uint8_t main_shape = shape & (UP_RIGHT | UP_LEFT | DOWN_LEFT | DOWN_RIGHT);
	switch (main_shape)
	{
		case UP_RIGHT: 					rt = "_UR"; break;
		case DOWN_LEFT: 				rt = "_DL"; break;
		case DIAG_SECOND:				rt = "_/"; break;
		case DIAG_MAIN:					rt = "_\\"; break;
		case UP_LEFT:					rt = "_UL"; break;
		case DOWN_RIGHT:				rt = "_DR"; break;
		case (UP_LEFT | DOWN_LEFT):		rt = "_3"; break;
		case (UP_RIGHT | DOWN_RIGHT):	rt = "_C"; break;
		case (UP_LEFT | UP_RIGHT):		rt = "_U"; break;
		case (DOWN_LEFT | DOWN_RIGHT):	rt = "_A"; break;

		case (UP_LEFT | UP_RIGHT | DOWN_LEFT | DOWN_RIGHT): rt = "_X"; break;
	}

	return rt;
}

Pixmap * Square::texture() const
{
	const Pixmap * p = nullptr;

	std::vector<Square *> n = neighbours();

	uint8_t fun_shape = 0, river_shape = 0, moist_shape = 0, rainy_shape = 0;

	for (int q = 0; q < 8; ++q)
	{
		if (n[q] == nullptr)
			continue;

		if (n[q]->vegetation() == V_FUNGUS)
			fun_shape |= 1 << q;

		if (((q % 2) == 0) && (n[q]->river()))
			river_shape |= 1 << q;

		if (n[q]->moist() > 0)
			moist_shape |= 1 << q;

		if (n[q]->moist() > 1)
			rainy_shape |= 1 << q;

	}

	if (m_alt >= 0)
		if (m_moist == 0)
			p = Resources::get()->getPixmap("pix::terrain::mud");
		else if (m_moist == 1)
		{
			const char * ms = analyze_shape(moist_shape);
			p = Resources::get()->getPixmap(std::string("pix::terrain::moist") + ms);
		}
		else	
		{
			const char * rs = analyze_shape(rainy_shape);
			p = Resources::get()->getPixmap(std::string("pix::terrain::rainy") + rs);
		}
//	else if (m_alt > -1000 && m_alt < 0)
//		p = Resources::get()->getPixmap("pix::terrain::water_shallow");
	else
		p = Resources::get()->getPixmap("pix::terrain::water_deep");

	Pixmap * op = nullptr;	
	if (p == nullptr)
		op = new Pixmap(56, 56);
	else
		op = p->clone();

	if (m_veg == V_FUNGUS)
	{
		const char * ft = analyze_shape(fun_shape);
		const char * et = "";
		if (m_alt < 0)
			et = "sea_";
		
		op->blit(Resources::get()->getPixmap(std::string("pix::terrain::") + et + "fungus" + ft));
	}
	else if(m_veg == V_FOREST)
	{
		op->blit(Resources::get()->getPixmap("pix::terrain::forest"));
	}
	if (m_alt > 0)
	{
		if (m_rock == 1)
			op->blit(Resources::get()->getPixmap("pix::terrain::rocky"));
		else if (m_rock == 2)
			op->blit(Resources::get()->getPixmap("pix::terrain::much_rocky"));
	}

	if (m_river == true && m_alt >= 0)
	{
		const char * rt = analyze_shape(river_shape);
		op->blit(Resources::get()->getPixmap(std::string("pix::terrain::river") + rt));
	}


	return op;
}

std::vector<Square *> Square::neighbours() const
{
	std::vector<Square *> rv;
	rv.push_back(up_right());
	rv.push_back(right());
	rv.push_back(down_right());
	rv.push_back(down());
	rv.push_back(down_left());
	rv.push_back(left());
	rv.push_back(up_left());
	rv.push_back(up());
	return rv;
}

Square * Square::up() const { return m_map->at(m_col, m_row - 2); }
Square * Square::down() const { return m_map->at(m_col, m_row + 2); }
Square * Square::left() const { return m_map->at(m_col - 2, m_row); }
Square * Square::right() const { return m_map->at(m_col + 2, m_row); }
Square * Square::up_left() const { return m_map->at(m_col - 1, m_row - 1); }
Square * Square::up_right() const { return m_map->at(m_col + 1, m_row - 1); }
Square * Square::down_left() const { return m_map->at(m_col - 1, m_row + 1); }
Square * Square::down_right() const { return m_map->at(m_col + 1, m_row + 1); }

/*void Square::update()
{
	m_map->addSquareTexture(this, texture());
}*/

Map::Map(unsigned width, unsigned height)
: m_width(width), m_height(height)
{
}

Map::~Map()
{
	for (auto & it : m_drawables)
	{
		delete it.second;
	}
	for (auto & row : m_squares)
	{
		for (auto & col : row.second)
		{
			delete col.second;
		}
	}
	/* todo - do cleanup */
}

Map * Map::create(unsigned width, unsigned height)
{
	Map * m = new Map(width, height);

	for (unsigned r = 0; r < height; ++r)
	{
		for (unsigned c = 0; c < width; ++c)
		{
			Square * s = new Square(m, c * 2 + (r & 1), r);
			m->m_squares[r][c * 2 + (r & 1)] = s;
		}
	}
	return m;
}

void Map::addSquareTexture(Square * square, Pixmap * texture)
{
	Drawables::iterator it = m_drawables.find(square);

	if (it == m_drawables.end())
	{
		RenderObject * ro = new RenderObject();

		ro->primitive = PrimitiveType::PointCloud;
		ro->view = 
			glm::lookAt( glm::vec3(0, 0, 0),
						 glm::vec3(0, 0, -100),
						 glm::vec3(0, 1, 0) );
		ro->translation    = glm::mat4( 1.0f );
		ro->translation    = glm::translate( glm::mat4( 1.0f ), glm::vec3(40.0f + (float) (square->col() * 40.0f), -20.0f + (float) ((m_height - square->row()) * 29.0f), 0.0f));
//		if ((square->row() & 1) == 1)
//			ro->translation = glm::translate( ro->translation, glm::vec3(40.0f, 0.0f, 0.0f));

		ro->translation    = glm::rotate( ro->translation, glm::radians(45.0f), glm::vec3(0, 0, 1));
		ro->translation    = glm::rotate( ro->translation, glm::radians(45.0f), glm::vec3(-1, 1, 0));
		ro->transformation = glm::mat4( 1.0f );

		m_drawables.insert(std::make_pair<>(square, ro));
		it = m_drawables.find(square);
	}
	else
	{
		platform->renderer().delete_vertex_data(it->second->vertices);
	}

	glm::vec3 * data_position;
	color * data_color;
	glm::vec3 * data_normal;
	unsigned n_points = pixmap_pointcloud(texture, &data_position, &data_color, &data_normal);
	delete texture;

	it->second->vertices = platform->renderer().upload_vertex_data(
		data_position,
		data_normal,
		data_color,
		nullptr,
		n_points);

	free(data_position);
	free(data_color);
	free(data_normal);
}

Square * Map::at(unsigned col, unsigned row)
{
	if (col < m_width * 2  && row < m_height)
	{
		Squares::iterator si = m_squares.find(row);
		if (si != m_squares.end())
		{
			Row::iterator ri = si->second.find(col);
			if (ri != si->second.end())
			{
				if (ri->second->row() != row || ri->second->col() != col)
				{
					printf("Should return square at [%d, %d] but returning square at [%d, %d]! Sorry...\n", col, row, ri->second->col(), ri->second->row());
				}
				return ri->second;
			}
		}
	}
	else
	{
//		printf("%d > %d or %d > %d\n", col, m_width, row, m_height);
	}
	return nullptr;
}

const RenderObjects Map::renderObjects() 
{
	RenderObjects out;
	for (unsigned row = 0; row < m_height; ++row)
		for (unsigned col = 0; col < m_width; ++col)
		{
			Square * s = at(col * 2 + (row & 1), row);
			if (!s)
				continue;
			if (s->dirty())
			{
				addSquareTexture(s, s->texture());
				s->clean();
			}
		}
	
	for (const auto & kv : m_drawables)
	{
		out.push_back(kv.second);
	}

	return out;
}

Map * Map::load(const std::string & fname)
{
	FILE * f = fopen(fname.c_str(), "rb");
	if (!f)
		return nullptr;

	MapHeader * header = (MapHeader *) malloc(sizeof(MapHeader));

	printf("sizeof(MapHeader) = %ld\n", sizeof(MapHeader));
	printf("sizeof(MapSquare) = %ld\n", sizeof(MapSquare));

	if (!fread(header, sizeof(MapHeader), 1, f))
	{
		fclose(f);
		free(header);
		return nullptr;
	}

	if (strcmp(header->sign, "TERRANMAP") != 0)
	{
		fclose(f);
		free(header);
		return nullptr;
	}
	
	printf("Creating map of %dx%d squares\n", header->width/2, header->height);
	Map * map = Map::create(header->width/2, header->height);
	
	MapSquare square;

//	int ctr = 0;

	for (int row = 0; row < header->height; ++row)
		for (int col = 0; col < header->width/2; ++col)
		{
			if (!fread(&square, sizeof(MapSquare), 1, f))
			{
				printf("Unable to read information for square [%d, %d]!\n", col, row);
				fclose(f);
				delete map;
				free(header);
				return nullptr;
			}
			Square * s = map->at(col * 2 + (row & 1), row);
			if (!s)
			{
				printf("Unable to obtain square for coords [%d, %d]\n", col, row);
			}
			s->altitude(10 + (square.altitude - 0x3C) * 30);
			uint8_t terrain = square.terrain_type;
//			printf("Terrain flags = %X\n", terrain);
			if ((terrain & SQUARE_MOIST) == SQUARE_MOIST)
				s->moist(1);
			else if ((terrain & SQUARE_RAINY) == SQUARE_RAINY)
				s->moist(2);

			uint8_t rfeat = square.terrain_flags1;
			if (rfeat & 0x40)
				s->rock(1);
			if (rfeat & 0x80)
				s->rock(2);

			uint32_t features = square.terrain_flags2;
			if ((features & FOREST) == FOREST)
				s->vegetation(V_FOREST);
			if ((features & FUNGUS) == FUNGUS)
				s->vegetation(V_FUNGUS);
			if ((features & RIVER) == RIVER || (features & SPRING) == SPRING)
				s->river(true);
//			printf("%d. Altitude = %d\n", ctr++, 10 + ((square.altitude - 0x3C) * 50));
//			return nullptr;
		}

	free(header);
	return map;
}
