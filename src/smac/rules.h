#pragma once

#include "txtfile.h"

namespace SMAC {

bool loadRules(const std::string & file);

class Facility {
	public:
		Facility(const StringArray & params);
		Facility();

	private:
		Facility operator=(const Facility & other);
		Facility(const Facility & other);

	protected:
		std::string m_name;
		std::string m_key;
		unsigned m_buildCost;
		unsigned m_maintenanceCost;
		std::string m_prerequisiteTechnology;
		std::string m_freeMaintenanceTechnology;



};

class Technology {
	public:
		Technology(const StringArray & params);
		Technology();

	private:
		Technology operator=(const Technology & other);
		Technology(const Technology & other);

	protected:
		std::string m_name;
		std::string m_key;
		unsigned m_conquerValue;
		unsigned m_discoverValue;
		unsigned m_buildValue;
		unsigned m_exploreValue;
		std::string m_prerequisites[2];
		unsigned short flags;
};

class Faction {
};

class Chassis {
};

class Weapon {
};

class Armor {
};

class Rules {
	public:
		Rules();

		void add(Technology & tech);
		void add(Facility & fac);
		void add(Faction & fac);
		void add(Chassis & chassis);
		void add(Armor & armor);
		void add(Weapon & weapon);
};

}
