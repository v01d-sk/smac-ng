#pragma once

#include <map>
#include <vector>

class Square;
class RenderObject;
class Pixmap;
class Map;

typedef std::map<unsigned, Square *> Row;
typedef std::map<unsigned, Row> Squares;
typedef std::vector<RenderObject *> RenderObjects;
typedef std::map<Square *, RenderObject *> Drawables;

enum Improvement { I_MIRROR, I_CONDENSER, I_BOREHOLE, I_BUNKER, I_COLLECTOR, I_MINE, I_PLATFORM, I_HARNESS };
enum Vegetation { V_FARM, V_FUNGUS, V_FOREST, V_NONE };

class Square {
	public:
		Square(Map * map, unsigned col, unsigned row): m_alt(10), m_col(col), m_row(row), m_moist(0), m_rock(0), m_improvement(0), m_veg(0), m_bonus(0), m_map(map), m_dirty(false), m_river(false) {}

		int altitude() const { return m_alt; }
		void altitude(int a) { m_alt = a; update(); }

		unsigned moist() const { return m_moist; }
		void moist(unsigned m) { m_moist = m; update(); }

		unsigned rock() const { return m_rock; }
		void rock(unsigned r) { m_rock = r; update(); }

		unsigned improvement() const { return m_improvement; }
		void improvement(unsigned i) { m_improvement = i; update(); }

		unsigned vegetation() const { return m_veg; }
		void vegetation(unsigned v) { m_veg = v; update(); }

		unsigned bonus() const { return m_bonus; }
		void bonus(unsigned b) { m_bonus = b; update(); }

		bool river() const { return m_river; }
		void river(bool r) { m_river = r; update(); }

		unsigned nutrient() const;
		unsigned mineral() const;
		unsigned energy() const;

		Pixmap * texture() const;

		unsigned col() const { return m_col; }
		unsigned row() const { return m_row; }

		void update() { m_dirty = true; }
		bool dirty() { return m_dirty; }
		void clean() { m_dirty = false; }

		Square * up() const; 
		Square * down() const; 
		Square * left() const; 
		Square * right() const; 
		Square * up_left() const; 
		Square * up_right() const; 
		Square * down_left() const; 
		Square * down_right() const; 
		std::vector<Square *> neighbours() const;

	protected:
		int m_alt;
		unsigned m_col;
		unsigned m_row;
		unsigned m_moist;
		unsigned m_rock;
		unsigned m_improvement; // probably needs different data type (mirror / condenser / borehole / bunker / collector / mine - platform / harness)
		unsigned m_veg;	// farm / fungus / forest / none
		unsigned m_bonus; // nutrient / mineral / energy
		Map * m_map;
		bool m_dirty;
		bool m_river;
};

class Map {
	protected:
		Map(unsigned width, unsigned height);

	public:
		static Map * create(unsigned width, unsigned height);
		static Map * load(const std::string & fname);
		static bool save(const std::string & fname);
		~Map();

		Square * at(unsigned col, unsigned row);
		const RenderObjects renderObjects();

		void addSquareTexture(Square * square, Pixmap * texture);

	protected:
		unsigned m_width;
		unsigned m_height;

		Squares     m_squares;
		Drawables	m_drawables;
};


