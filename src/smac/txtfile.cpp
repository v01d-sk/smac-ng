#include "txtfile.h"
#include <string.h>

using namespace SMAC;

bool SMAC::loadTextFile(const std::string & file, TextData & output)
{
	std::string record;
	std::string class_name("");
	FILE * f = fopen(file.c_str(), "r");
	char line[256];
	if (!f)
	{
		return false;
	}

	while (!feof(f))
	{
		fgets(line, sizeof(line) - 1, f);
		// remove final newline
		line[strlen(line) - 1] = 0;
		if (line[strlen(line) - 1] == '\r')
			line[strlen(line) - 1] = 0;
		char * semicolon = strchr(line, ';');

		// ignore comments
		if (semicolon != NULL) *semicolon = 0;

		if (strlen(line) == 0)
			// ignore empty strings
			continue;

		if (line[0] == '#')
		{
		   if (line[1] != '#' && line[1] != ' ')
		   {
			   // start of rule class
			   class_name = std::string(&line[1]);
			   printf("Class name = %s\n", class_name.c_str());

		   }
		}
		else
		{
			output[class_name].push_back(line);
			printf("Data = \"%s\"\n", &line[0]);

		}
	}

	return true;
}
